package io.automata.matashift;

import javax.inject.Singleton;

import dagger.Component;
import io.automata.matashift.api.GameApi;

/**
 * Created by Andy on 2016/1/14.
 */
@Singleton
@Component(modules={AppModule.class})
public interface AppComponent {

    void inject(MainActivity activity);
    void inject(WindowService view);

    GameApi providesGameApi();

}
