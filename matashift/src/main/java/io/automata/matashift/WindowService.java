package io.automata.matashift;

import android.view.LayoutInflater;

import javax.inject.Inject;

import io.automata.common.CommonWindowService;
import io.automata.common.ui.CommonMainView;
import io.automata.matashift.api.GameApi;

public class WindowService extends CommonWindowService {

    @Inject GameApi mGameApi;

    private static final String DEBUG_URL = "http://auto.elggum.com/menu-matashift.html";
    private static final String RELEASE_URL = "http://auto.elggum.com/matashift_030.html";

    public CommonMainView getMainWindowView() {
        ((MainApp) getBaseContext().getApplicationContext()).getAppComponent().inject(this);
        LayoutInflater layoutInflater = LayoutInflater.from(getApplicationContext());
        CommonMainView cmv = (CommonMainView) layoutInflater.inflate(R.layout.autometa_main, null, false);
        if (BuildConfig.DEBUG) {
            cmv.init(CommonMainView.GAME_TYPE_MATASHIFT, mGameApi, DEBUG_URL);
        } else {
            cmv.init(CommonMainView.GAME_TYPE_MATASHIFT, mGameApi, RELEASE_URL);
        }
        return cmv;
    }

}
