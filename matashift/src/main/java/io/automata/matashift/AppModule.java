package io.automata.matashift;

import android.app.Application;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.automata.matashift.api.GameApi;

/**
 * Created by Andy on 2016/1/14.
 */
@Module
public class AppModule {

    Application mApplication;

    public AppModule(Application application) {
        mApplication = application;
    }

    @Provides @Singleton Application providesApplication() {
        return mApplication;
    }

    @Provides @Singleton GameApi providesGameApi() {
        return new GameApi(mApplication);
    }

}
