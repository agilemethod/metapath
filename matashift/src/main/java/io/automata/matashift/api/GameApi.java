package io.automata.matashift.api;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import javax.inject.Inject;

import io.automata.common.api.CommonGameApi;

/**
 * Created by Andy on 2016/1/8.
 */

public class GameApi extends CommonGameApi {

    static {
        System.loadLibrary("matashift");
    }

    @Inject public GameApi(Context context) {
        init(context);
    }

    public native void init(Context context);

    public native void requestBase64(String base64);

    public native String call(String command);

}
