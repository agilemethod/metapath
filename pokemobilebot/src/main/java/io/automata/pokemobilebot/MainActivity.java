package io.automata.pokemobilebot;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RelativeLayout;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import io.automata.pokemobilebot.api.GameApi;

public class MainActivity extends AppCompatActivity {

    static private final long AD_UPDATE_TIME = 45 * 1000;

    GameApi mGameApi;
    String mURL1 = "file:///android_asset/index.html";
    private String mAdId0;
    private String mAdId1;
    private long mLastUpdateAdTime = 0;
    private AdRequest mAdRequest;
    private Handler mHandlerTime = new Handler(Looper.getMainLooper());
    private boolean mIsWebViewLoaded = false;
    private Tracker mTracker;

    InterstitialAd mInterstitialAd;
    AdView mAdView2;
    WebView mWebView;
    RelativeLayout mViewContainer;
    RelativeLayout mAdContainer2;

    private AdListener mAdListener = new AdListener() {

        @Override public void onAdLoaded() {

        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getDefaultTracker();

        mGameApi = new GameApi(this);

        mAdId0 = getAdId0();
        mAdId1 = getAdId1();

        mWebView = (WebView) findViewById(R.id.browser);
        mAdContainer2 = (RelativeLayout) findViewById(R.id.adViewContainer2);
        mViewContainer = (RelativeLayout) findViewById(R.id.viewcontainer);

        mAdView2 = newAdView(mAdId1);
        mAdContainer2.addView(mAdView2);

        mInterstitialAd = newInterstitialAd(mAdId0);

        mAdView2.setAdListener(mAdListener);
        mInterstitialAd.setAdListener(new AdListener() {
            @Override public void onAdLoaded() {
                mInterstitialAd.show();
            }
            @Override public void onAdClosed() {
                if (!mIsWebViewLoaded) {
                    sendTrackerScreen("WebView");
                    loadWebView();
                }
            }
        });

        reloadAd();
        mHandlerTime.postDelayed(showAd, 300 * 1000);
        sendTrackerScreen("MainActivity");
    }

    private String getAdId0() {
        return "ca-app-pub-" + mGameApi.call("{\"request\":\"get_ad_id0\"}");
    }

    private String getAdId1() {
        return "ca-app-pub-" + mGameApi.call("{\"request\":\"get_ad_id1\"}");
    }

    private InterstitialAd newInterstitialAd(String id) {
        InterstitialAd interstitialAd = new InterstitialAd(this);
        interstitialAd.setAdUnitId(id);
        return interstitialAd;
    }

    private AdView newAdView(String id) {
        AdView adView = new AdView(this);
        adView.setAdSize(AdSize.FLUID);
        adView.setAdUnitId(id);
        return adView;
    }

    private void reloadAd() {
        if (System.currentTimeMillis() - mLastUpdateAdTime > AD_UPDATE_TIME) {
            mAdRequest = new AdRequest.Builder().build();
            mAdView2.loadAd(mAdRequest);
            mInterstitialAd.loadAd(mAdRequest);
            mLastUpdateAdTime = System.currentTimeMillis();
        }
    }

    void loadWebView() {
        WebSettings websettings = mWebView.getSettings();
        websettings.setSupportZoom(true);
        websettings.setBuiltInZoomControls(true);
        websettings.setJavaScriptEnabled(true);
        websettings.setDomStorageEnabled(true);
        mWebView.setWebViewClient(new WebViewClient());
        mWebView.loadUrl(mURL1);
        mIsWebViewLoaded = true;
    }

    private final Runnable showAd = new Runnable()
    {
        public void run()
        {
            reloadAd();
            mInterstitialAd.show();
            mHandlerTime.postDelayed(this, 300 * 1000);
        }
    };

    public void sendTrackerEvent(String action, String category, String label) {
        mTracker.send(new HitBuilders.EventBuilder()
            .setCategory(category)
            .setAction(action)
            .setLabel(label)
            .build());
    }

    public void sendTrackerScreen(String screenName) {
        mTracker.setScreenName(screenName);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    synchronized public Tracker getDefaultTracker() {
        if (mTracker == null) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            mTracker = analytics.newTracker("UA-77564871-7");
        }
        return mTracker;
    }

}
