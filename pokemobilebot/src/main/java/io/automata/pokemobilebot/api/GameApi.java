package io.automata.pokemobilebot.api;

import android.content.Context;

/**
 * Created by Andy on 2016/1/8.
 */

public class GameApi {

    public interface RespondListener {

        void sendTrackerScreen(String n);
        void sendTrackerEvent(String a, String c, String l);

    }

    public RespondListener listener;

    static {
        System.loadLibrary("pokemobilebot");
    }

    public GameApi(Context context) {
        init(context);
    }

    public native void init(Context context);

    public native String call(String command);

    public void sendTrackerScreen(final String n) {
        if (listener != null) {
            listener.sendTrackerScreen(n);
        }
    }

    public void sendTrackerEvent(final String a, final String c, final String l) {
        if (listener != null) {
            listener.sendTrackerEvent(a, c, l);
        }
    }

}
