//
// Created by Andy on 2016/2/17.
//

#ifndef METAPATH_BASE64_H
#define METAPATH_BASE64_H
#include <string>

std::string base64_encode(unsigned char const* , unsigned int len);
std::string base64_decode(std::string const& s);
#endif //METAPATH_BASE64_H
