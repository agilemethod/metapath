LOCAL_PATH := $(call my-dir)

#=================================================================#

include $(CLEAR_VARS)

OPENCV_LIB_TYPE:=STATIC

LOCAL_SRC_FILES := \
  src/pokemonbrowser/gameplayer.cpp \
  src/base64.cpp \
  main.cpp

LOCAL_MODULE := pokemobilebot
LOCAL_LDLIBS += -llog -ldl
LOCAL_C_INCLUDES += $(LOCAL_PATH)/include $(LOCAL_PATH)/src

LOCAL_CFLAGS := -std=c++11 -DNDEBUG -O2

include $(BUILD_SHARED_LIBRARY)
