//
// Created by Andy on 2016/1/4.
//
#include "jni.h"

#ifndef METAPATH_MAIN_H
#define METAPATH_MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

JNIEXPORT void JNICALL Java_io_automata_pokemobilebot_api_GameApi_init(
        JNIEnv *env, jobject obj, jobject thiz);

JNIEXPORT jstring JNICALL Java_io_automata_pokemobilebot_api_GameApi_call(
        JNIEnv *env, jobject obj, jstring cmd);

#ifdef __cplusplus
}

#endif
#endif //METAPATH_MAIN_H
