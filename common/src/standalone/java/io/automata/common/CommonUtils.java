package io.automata.common;

import android.util.Log;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;

/**
 * Created by Andy on 2016/7/9.
 */
public class CommonUtils {

    public static boolean chmodInputEvent() {
        Log.d("Matapath", "Using standalone");
        try {
            Process process = Runtime.getRuntime().exec("su");
            DataOutputStream os = new DataOutputStream(process.getOutputStream());
            os.writeBytes("echo root_test\n");
            os.writeBytes("setenforce 0\n");
            os.writeBytes("chmod 666 /dev/input/event*\n");
            os.writeBytes("exit\n");
            os.flush();
            os.close();
            process.waitFor();
            BufferedReader stdin = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String s = stdin.readLine();
            if (s != null && s.contains("root_test")) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

}
