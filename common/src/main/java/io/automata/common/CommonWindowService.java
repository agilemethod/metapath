package io.automata.common;

import android.app.Service;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.os.IBinder;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;

import io.automata.common.ui.CommonMainView;

abstract public class CommonWindowService extends Service {

    private WindowManager mWindowManager;
    private CommonMainView mMainMenu;
    private boolean mBtnMovable = true;

    public abstract CommonMainView getMainWindowView();

    @Override public IBinder onBind(Intent intent) {
        return null;
    }

    @Override public void onCreate() {
        super.onCreate();
        Log.d("CommonWindowService", "onCreate!");
        mWindowManager = (WindowManager) getSystemService(WINDOW_SERVICE);

        mMainMenu = getMainWindowView();
        mMainMenu.setOnCloseListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                stopSelf();
            }
        });

        Point size = new Point();
        mWindowManager.getDefaultDisplay().getSize(size);
        final WindowManager.LayoutParams params1 = new WindowManager.LayoutParams(
            (int) getResources().getDimension(R.dimen.g_btn_height),
            (int) getResources().getDimension(R.dimen.g_btn_height),
            WindowManager.LayoutParams.TYPE_PHONE,
            WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH,
            PixelFormat.TRANSLUCENT);

        params1.gravity = Gravity.TOP | Gravity.LEFT;
        params1.x = 0;
        params1.y = 0;
        params1.screenOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;

        final WindowManager.LayoutParams params2 = new WindowManager.LayoutParams(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.TYPE_PHONE,
            WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH,
            PixelFormat.TRANSLUCENT);

        params2.gravity = Gravity.TOP | Gravity.LEFT;
        params2.x = 0;
        params2.y = 0;
        params2.screenOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;

        mMainMenu.setOnShowListener(new View.OnClickListener(){
            @Override public void onClick(View v) {
                mWindowManager.updateViewLayout(mMainMenu, params2);
                mBtnMovable = false;
            }
        });

        mMainMenu.setOnHideListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                mWindowManager.updateViewLayout(mMainMenu, params1);
                mBtnMovable = true;
            }
        });

        final Point screenSize = size;
        final int btnWidth = (int) getResources().getDimension(R.dimen.g_btn_width);
        mMainMenu.setMainBtnOnTouchListener(new View.OnTouchListener() {
            @Override public boolean onTouch(View v, MotionEvent event) {
                int touchX = (int) event.getRawX() - btnWidth / 2;
                if (touchX <  btnWidth / 2) touchX = 0;
                if (touchX > screenSize.x - btnWidth) touchX = screenSize.x - btnWidth;
                if (mBtnMovable && Math.abs(touchX - params1.x) > btnWidth / 2) {
                    params1.x = touchX;
                    mWindowManager.updateViewLayout(mMainMenu, params1);
                }
                return false;
            }
        });

        mWindowManager.addView(mMainMenu, params1);
    }

    @Override public void onDestroy() {
        super.onDestroy();
        if (mMainMenu != null) {
            mWindowManager.removeView(mMainMenu);
        }
    }

}
