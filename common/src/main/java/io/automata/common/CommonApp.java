package io.automata.common;

import android.app.Application;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

/**
 * Created by Andy on 2016/1/14.
 */
public class CommonApp extends Application {

    private Tracker mTracker;

    @Override public void onCreate() {
        super.onCreate();
        getDefaultTracker();
    }

    public void sendTrackerEvent(String action, String category, String label) {
        mTracker.send(new HitBuilders.EventBuilder()
            .setCategory(category)
            .setAction(action)
            .setLabel(label)
            .build());
    }

    public void sendTrackerScreen(String screenName) {
        mTracker.setScreenName(screenName);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    synchronized public Tracker getDefaultTracker() {
        if (mTracker == null) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
            mTracker = analytics.newTracker("UA-77564871-4");
        }
        return mTracker;
    }

}
