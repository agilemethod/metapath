package io.automata.common;

import android.Manifest;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

abstract public class CommonActivity extends AppCompatActivity {

    private CommonApp mApplication;
    public static String TAG = "AutomataCommon";
    public static int OVERLAY_PERMISSION_REQ_CODE = 2428;
    public static int WRITE_PERMISSION_REQ_CODE = 2429;
    public boolean mIsAllowWindow = false;

    public abstract Class getWindowService();

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mApplication = (CommonApp) getApplication();
        mApplication.sendTrackerScreen(getClass().getSimpleName());

        setContentView(R.layout.activity_main);
        RelativeLayout r = (RelativeLayout) findViewById(R.id.main_view);
        final TextView textView = (TextView) findViewById(R.id.main_info_text);
        r.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                Log.d(TAG, "Try to get root");
                if (!checkAPK()) {
                    textView.setText("此APK遭人修改，請從Matapath粉絲團或Google Play下載");
                    mApplication.sendTrackerEvent("Click", "MainActivityEvent", "Check APK Failure");
                } else if (!mIsAllowWindow) {
                    textView.setText("請允許浮動視窗權限（For Android 6.0 up）");
                    mApplication.sendTrackerEvent("Click", "MainActivityEvent", "NotAllowWindow");
                } else if (CommonUtils.chmodInputEvent()) {
                    Log.d(TAG, "Get root success");
                    startService(new Intent(getBaseContext(), getWindowService()));
                    textView.setText("取得Root權限成功，請盡情享受神魔之塔");
                    mApplication.sendTrackerEvent("Click", "MainActivityEvent", "GetRootSuccess");
                } else {
                    Log.d(TAG, "Get root failure");
                    textView.setText("無法取得Root權限，請確定手機是否已經Root，\n並且允許SuperSU授與Matapath權限");
                    mApplication.sendTrackerEvent("Click", "MainActivityEvent", "GetRootFailure");
                }
            }
        });
        if (android.os.Build.VERSION.SDK_INT >= 23) {
            if (!Settings.canDrawOverlays(this)) {
                mIsAllowWindow = false;
                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                    Uri.parse("package:" + getPackageName()));
                startActivityForResult(intent, OVERLAY_PERMISSION_REQ_CODE);

            } else {
                mIsAllowWindow = true;
            }
            requestPermission();
        } else {
            mIsAllowWindow = true;
        }
    }

    private boolean checkAPK() {
        if (BuildConfig.DEBUG) {
            return true;
        }
        try {
            Signature sigs = getPackageManager()
                .getPackageInfo(getPackageName(), PackageManager.GET_SIGNATURES).signatures[0];
            if (sigs.toCharsString().contains("60355040b130e417")
                && sigs.toCharsString().contains("54abc2596118c43c939cd1d0a143")) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    private boolean checkManifest() {
        try {
            List<PackageInfo> pInfos = getPackageManager().getInstalledPackages(PackageManager.GET_ACTIVITIES);
            for (PackageInfo pInfo : pInfos) {
                if (!pInfo.packageName.contains("io.automata")) {
                    continue;
                }
                ActivityInfo[] aInfos = pInfo.activities;
                if (aInfos != null) {
                    for (ActivityInfo activityInfo : aInfos) {
                        if (activityInfo.name.contains("com.google.android.gms.ads.AdActivity")) {
                            return true;
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    private void requestPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (android.os.Build.VERSION.SDK_INT >= 16)
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
        }
    }

    @Override protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (android.os.Build.VERSION.SDK_INT >= 23) {
            if (requestCode == OVERLAY_PERMISSION_REQ_CODE) {
                if (!Settings.canDrawOverlays(this)) {
                    mIsAllowWindow = false;
                } else {
                    mIsAllowWindow = true;
                }
            }
        } else {
            mIsAllowWindow = true;
        }
    }

    @Override protected void onPause() {
        super.onPause();
    }

    @Override protected void onResume() {
        super.onResume();
    }

}
