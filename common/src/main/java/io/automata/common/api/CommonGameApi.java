package io.automata.common.api;

import android.content.Context;

/**
 * Created by Andy on 2016/1/8.
 */

abstract public class CommonGameApi {

    public interface RespondListener {

        void sendTrackerScreen(String n);
        void sendTrackerEvent(String a, String c, String l);

    }

    public CommonGameApi() {}

    public RespondListener listener;

    public abstract void init(Context context);

    public abstract void requestBase64(String base64);

    public abstract String call(String command);

    public void sendTrackerScreen(final String n) {
        if (listener != null) {
            listener.sendTrackerScreen(n);
        }
    }

    public void sendTrackerEvent(final String a, final String c, final String l) {
        if (listener != null) {
            listener.sendTrackerEvent(a, c, l);
        }
    }

}
