package io.automata.common.ui;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.graphics.Color;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;

import io.automata.common.CommonApp;
import io.automata.common.R;
import io.automata.common.api.CommonGameApi;

public class CommonMainView extends RelativeLayout {

    static public final int GAME_TYPE_MATAPATH = 0;
    static public final int GAME_TYPE_MATASHIFT = 1;

    static private final long AD_CLICK_HIDE_DURING = 6 * 60 * 60 * 1000;
    static private final long AD_UPDATE_TIME = 45 * 1000;
    static private final int AD_SHOWING_TIME = 6;
    static private final long AD_HIDE_TIME = 2 * 60 * 1000;
    static private final long AD_CHECK_TIME = 24 * 60 * 60 * 1000; // 1 day
    static private final long AD_START_CHECK_TIMES = 2;
    static private final String AD_LAST_CLICK_PREF_NAME = "AD_LAST_CLICK_TIME_NAME";
    static private final String AD_PREFERENCE_NAME = "automata_shared_preferences";

    CommonGameApi mCommonGameApi;

    ImageButton mMainBtn;
    LinearLayout mSubMenu;
    ImageButton mStartBtn;
    ImageButton mStopBtn;
    ImageButton mCloseBtn;
    ImageButton mFun1Btn;
    ImageButton mFun2Btn;
    WebView mWebView;
    AdView mAdView;
    AdView mAdView2;
    RelativeLayout mAdContainer;
    RelativeLayout mAdContainer2;
    TextView mTimerText;
    OnClickListener mOnCloseListener;
    OnClickListener mOnShowListener;
    OnClickListener mOnHideListener;
    OnTouchListener mMainBtnOnTouchListener;
    String mURL1;
    boolean mAdShowing = false;
    boolean mWebViewIsShowing = false;

    private String mAdId;
    private SharedPreferences mPreferences;
    private Timer mTimer;
    private int mAdTimes;
    private Handler mHandler = new Handler(Looper.getMainLooper());
    private CommonApp mApp;
    private AdRequest mAdRequest;
    private long mLastUpdateAdTime = 0;
    private boolean mIsLoad = false;
    private boolean mIsSize = false;
    private long mStartTime = 0;
    private long mCheckTimes = 0;

    public int mGameType = 0;

    public CommonMainView(Context context) {
        this(context, null);
    }

    public CommonMainView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CommonMainView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    private AdListener mAdListener = new AdListener() {

        @Override public void onAdLeftApplication() {
            super.onAdLeftApplication();
            mPreferences.edit().putLong(AD_LAST_CLICK_PREF_NAME, System.currentTimeMillis()).commit();
            mApp.sendTrackerEvent("Click", "Ad", "Click");
        }

        @Override public void onAdLoaded() {
            mIsLoad = true;
        }

    };

    @Override public void onFinishInflate() {
        super.onFinishInflate();
    }

    public void init(int gameType, CommonGameApi api, String menu) {
        mCommonGameApi = api;
        mURL1 = menu;
        mGameType = gameType;
        onFinishInflateAfterInit();
    }

    public void onFinishInflateAfterInit() {
        mApp = (CommonApp) getContext().getApplicationContext();
        mApp.sendTrackerScreen(this.getClass().getSimpleName());

        bindView();
        loadWebView();

        mAdId = getAdId();
        mAdView = newAdView(mAdId);
        mAdView2 = newAdView(mAdId);
        mAdContainer.addView(mAdView);
        mAdContainer2.addView(mAdView2);

        ViewGroup.LayoutParams layout_params = mAdContainer.getLayoutParams();
        layout_params.width = LayoutParams.MATCH_PARENT;
        layout_params.height = LayoutParams.WRAP_CONTENT;
        ViewGroup.LayoutParams layout_params2 = mAdContainer2.getLayoutParams();
        layout_params2.width = LayoutParams.MATCH_PARENT;
        layout_params2.height = LayoutParams.WRAP_CONTENT;

        mAdContainer.setLayoutParams(layout_params);
        mAdContainer2.setLayoutParams(layout_params2);

        // init adView
        reloadAd();
        bindAction();

        mPreferences = getContext().getSharedPreferences(AD_PREFERENCE_NAME, 0);
        mStartTime = System.currentTimeMillis();
    }

    private void reloadAd() {
        if (System.currentTimeMillis() - mLastUpdateAdTime > AD_UPDATE_TIME) {
            mAdRequest = new AdRequest.Builder().build();
            mAdView.loadAd(mAdRequest);
            mAdView2.loadAd(mAdRequest);
            mLastUpdateAdTime = System.currentTimeMillis();
        }
    }

    private AdView newAdView(String id) {
        AdView adView = new AdView(getContext());
        adView.setAdSize(AdSize.FLUID);
        adView.setAdUnitId(id);
        return adView;
    }

    private String getAdId() {
        return "ca-app-pub-" + mCommonGameApi.call("{\"request\":\"get_ad_id\"}");
    }

    private void bindView() {
        mMainBtn = (ImageButton) findViewById(R.id.btn_main);
        mSubMenu = (LinearLayout) findViewById(R.id.sub_menu);
        mStartBtn = (ImageButton) findViewById(R.id.btn_start);
        mStopBtn = (ImageButton) findViewById(R.id.btn_stop);
        mCloseBtn = (ImageButton) findViewById(R.id.btn_close);
        mFun1Btn = (ImageButton) findViewById(R.id.btn_fun1);
        mFun2Btn = (ImageButton) findViewById(R.id.btn_fun2);
        mWebView = (WebView) findViewById(R.id.browser);
        mAdContainer = (RelativeLayout) findViewById(R.id.adViewContainer);
        mAdContainer2 = (RelativeLayout) findViewById(R.id.adViewContainer2);
        mTimerText = (TextView) findViewById(R.id.timer_text);
    }

    void bindAction() {
        mMainBtn.setOnClickListener(new OnClickListener() {
            @Override public void onClick(View v) {
                toggleDisplayView();
            }
        });
        mStartBtn.setOnClickListener(new OnClickListener() {
            @Override public void onClick(View v) {
                String call = String.format("javascript:cmd('%s')", "Start");
                mWebView.loadUrl(call);
                mApp.sendTrackerEvent("Click", "WindowButton", "Start");
            }
        });
        mStopBtn.setOnClickListener(new OnClickListener() {
            @Override public void onClick(View v) {
                String call = String.format("javascript:cmd('%s')", "Stop");
                mWebView.loadUrl(call);
                mApp.sendTrackerEvent("Click", "WindowButton", "Stop");
            }
        });
        mFun1Btn.setOnClickListener(new OnClickListener() {
            @Override public void onClick(View v) {
                String call = String.format("javascript:cmd('%s')", "Fun1");
                mWebView.loadUrl(call);
            }
        });
        mFun2Btn.setOnClickListener(new OnClickListener() {
            @Override public void onClick(View v) {
                loadPlayer();
                mApp.sendTrackerEvent("Click", "WindowButton", "Reload");
                String call = String.format("javascript:cmd('%s')", "Reload");
                mWebView.loadUrl(call);
            }
        });
        mCloseBtn.setOnClickListener(new OnClickListener() {
            @Override public void onClick(View v) {
                if (mOnCloseListener != null) {
                    mOnCloseListener.onClick(v);
                    mApp.sendTrackerEvent("Click", "WindowButton", "Close");
                }
            }
        });
        mAdView.setAdListener(mAdListener);
        mAdView2.setAdListener(mAdListener);
        mWebView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override public void onGlobalLayout() {
                int newVis = mWebView.getVisibility();
                if (newVis == GONE && mWebViewIsShowing) {
                    String call = String.format("javascript:cmd('%s')", "DidHide");
                    mWebView.loadUrl(call);
                    mWebViewIsShowing = false;
                } else if (newVis == VISIBLE && !mWebViewIsShowing) {
                    String call = String.format("javascript:cmd('%s')", "DidShow");
                    mWebView.loadUrl(call);
                    mWebViewIsShowing = true;
                }
            }
        });
    }

    void setPlaying(Boolean playing) {
        if (playing) {
            mMainBtn.setBackgroundResource(R.drawable.ic_menu_play);
            mStartBtn.setVisibility(GONE);
            mStopBtn.setVisibility(VISIBLE);
        } else {
            mMainBtn.setBackgroundResource(R.drawable.ic_menu_stop);
            mStartBtn.setVisibility(VISIBLE);
            mStopBtn.setVisibility(GONE);
        }
        if (mGameType == GAME_TYPE_MATASHIFT) {
            mStartBtn.setVisibility(GONE);
            mStopBtn.setVisibility(GONE);
        }
    }

    void toggleDisplayView() {
        if (mSubMenu.getVisibility() == VISIBLE) {
            hideDisplayView();
        } else {
            showDisplayView();
        }
    }

    void hideDisplayView() {
        mSubMenu.setVisibility(GONE);
        mWebView.setVisibility(GONE);
        hideAd();
        if (mOnHideListener != null) {
            mOnHideListener.onClick(null);
        }
    }

    void showDisplayView() {
        if (mOnShowListener != null) {
            mOnShowListener.onClick(null);
        }
        mSubMenu.setVisibility(VISIBLE);
        mWebView.setVisibility(VISIBLE);
        checkAndShowAd();
        showAd();
        reloadAd();
        checkAd();
    }

    void checkAd() {
        if (mIsLoad && mIsSize) {
            return;
        }
        long lastClickTime = mPreferences.getLong(AD_LAST_CLICK_PREF_NAME, 0);
        if (System.currentTimeMillis() - lastClickTime < AD_CLICK_HIDE_DURING) {
            return;
        }
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override public void run() {
                mCheckTimes++;
                if (mAdView.getWidth() > 200 && mAdView.getHeight() > 40) {
                    mIsSize = true;
                }
                if (mAdView2.getWidth() > 200 && mAdView2.getHeight() > 40) {
                    mIsSize = true;
                }
                //Log.d("Matapath", "Check Ad " + mIsLoad + " " + mIsSize + " " + mCheckTimes);
                if (mIsLoad && mIsSize) {
                    return;
                }
                if (System.currentTimeMillis() - mStartTime > AD_CHECK_TIME
                    && mCheckTimes >= AD_START_CHECK_TIMES) {
                    mSubMenu.setVisibility(GONE);
                    mWebView.setVisibility(GONE);
                    Log.d("Matapath", "Error ad is gone !!??");
                    mApp.sendTrackerEvent("Visible", "Ad", "ERROR! Ad is gone?");
                    Toast.makeText(getContext(), "廣告無法開啟，將關閉程式", Toast.LENGTH_LONG).show();
                    new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                        @Override public void run() {
                            if (mOnCloseListener != null) {
                                mOnCloseListener.onClick(null);
                            } else {
                                System.exit(0);
                            }
                        }
                    }, 1000);
                }
            }
        }, 1000);
    }

    void checkAndShowAd() {
        long nowTime = System.currentTimeMillis();
        if (nowTime - mStartTime < AD_HIDE_TIME) {
            mAdContainer.setVisibility(GONE);
            return;
        }
        mPreferences = getContext().getSharedPreferences(AD_PREFERENCE_NAME, 0);
        long lastClickTime = mPreferences.getLong(AD_LAST_CLICK_PREF_NAME, 0);
        if (lastClickTime > nowTime) {
            lastClickTime = 0;
        }
        if (nowTime - lastClickTime < AD_CLICK_HIDE_DURING) {
            mTimerText.setVisibility(GONE);
            long hideTime = AD_CLICK_HIDE_DURING - (nowTime - lastClickTime);
            SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss", Locale.US);
            formatter.setTimeZone(TimeZone.getTimeZone("GMT+0"));
            mTimerText.setText(formatter.format(new Date(hideTime)));
        } else {
            if (mTimer != null) {
                mTimer.cancel();
            }
            mApp.sendTrackerEvent("Visible", "Ad", "ShowAd1");
            mAdContainer.setVisibility(VISIBLE);
            mAdTimes = AD_SHOWING_TIME;
            mTimer = new Timer(true);
            mTimer.schedule(new TimerTask() {
                @Override public void run() {
                    mHandler.post(new Runnable() {
                        @Override public void run() {
                            mTimerText.setText(String.format(Locale.US, "%d", mAdTimes));
                            if (mAdTimes == 0) {
                                mTimerText.setVisibility(GONE);
                                mAdContainer.setVisibility(GONE);
                                mTimer.cancel();
                            }
                            mAdTimes--;
                        }
                    });
                }
            }, 0, 1000);
        }
    }

    void hideAd() {
        mAdContainer2.setVisibility(GONE);
    }

    void showAd() {
        if (mAdShowing) {
            mApp.sendTrackerEvent("Visible", "Ad", "ShowAd2");
            mAdContainer2.setVisibility(VISIBLE);
        }
    }

    void loadPlayer() {
        ConnectivityManager cm = (ConnectivityManager) getContext()
            .getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo i = cm.getActiveNetworkInfo();
        if ((i == null) || (!i.isConnected())) {

        } else {
            mWebView.loadUrl(mURL1);
        }
    }

    void loadWebView() {
        WebSettings websettings = mWebView.getSettings();
        websettings.setSupportZoom(true);
        websettings.setBuiltInZoomControls(true);
        websettings.setJavaScriptEnabled(true);
        websettings.setDomStorageEnabled(true);
        mWebView.setWebViewClient(new WebViewClient());
        loadPlayer();
        mWebView.addJavascriptInterface(new JSHandler(this), "Bridge");
        mWebView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                view.setBackgroundColor(Color.TRANSPARENT);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    view.setLayerType(View.LAYER_TYPE_HARDWARE, null);
                } else {
                    view.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                }
            }
        });
        mWebView.setBackgroundColor(Color.TRANSPARENT);
    }

    public void setOnShowListener(OnClickListener l) {
        mOnShowListener = l;
    }

    public void setMainBtnOnTouchListener(OnTouchListener l) {
        mMainBtnOnTouchListener = l;
        mMainBtn.setOnTouchListener(mMainBtnOnTouchListener);
    }

    public void setOnHideListener(OnClickListener l) {
        mOnHideListener = l;
    }
    public void setOnCloseListener(OnClickListener l) {
        mOnCloseListener = l;
    }

    private static class JSHandler {

        CommonMainView mView;
        Handler mHandler;
        String mMessage = "";
        int mVersionCode = 0;

        CommonGameApi.RespondListener mlistener = new CommonGameApi.RespondListener() {
            @Override public void sendTrackerScreen(final String n) {
                mView.mApp.sendTrackerScreen(n);
            }

            @Override public void sendTrackerEvent(final String a, final String c, final String l) {
                mView.mApp.sendTrackerEvent(a, c, l);
            }
        };

        public JSHandler(CommonMainView view) {
            mView = view;
            mView.mCommonGameApi.listener = mlistener;
            mView = view;
            mHandler = new Handler(Looper.getMainLooper());
        }

        @JavascriptInterface
        public String call(final String cmd) {
            return mView.mCommonGameApi.call(cmd);
        }

        @JavascriptInterface
        public void callAutomata(String base64) {
            Log.d("JSHandler msg!!!!!!", base64);
            mView.mCommonGameApi.requestBase64(base64);
        }

        @JavascriptInterface
        public String callApp(final String action) {
            Log.d("CallApp", action);
            if (action.equals("getWidthHeight")) {
                Context c = mView.getContext();
                WindowManager w = (WindowManager) c.getSystemService(Context.WINDOW_SERVICE);
                Point size = new Point();
                w.getDefaultDisplay().getSize(size);
                return String.format("[%d, %d]", size.x, size.y);
            } else if (action.equals("getWebViewXY")) {
                return String.format("[%d, %d]", mView.mWebView.getLeft(), mView.mWebView.getTop());
            } else if (action.contains("trackScreen")) {
                mView.mApp.sendTrackerScreen(action);
            } else if (action.contains("trackEvent")) {
                mView.mApp.sendTrackerEvent("event", "webviewEvent", action);
            } else if (action.equals("getVersionName")) {
                try {
                    PackageInfo pInfo = mView.getContext().getPackageManager()
                        .getPackageInfo(mView.getContext().getPackageName(), 0);
                    return pInfo.versionName;
                } catch (Exception e) {
                    return "";
                }
            } else if (action.equals("getVersionCode")) {
                try {
                    PackageInfo pInfo = mView.getContext().getPackageManager()
                        .getPackageInfo(mView.getContext().getPackageName(), 0);
                    mVersionCode = pInfo.versionCode;
                    return Integer.toString(pInfo.versionCode);
                } catch (Exception e) {
                    return "";
                }
            } else if (action.equals("getExternalPath")) {
                return Environment.getExternalStorageDirectory().getAbsolutePath();
            } else if (action.equals("getUserFilePath")) {
                //File f = mView.getContext().getExternalFilesDir(null);
                File f = mView.getContext().getFilesDir();
                if (f != null) {
                    return f.getAbsolutePath();
                }
                return "";
            } else if (action.contains("getUiStatus")) {
                return String.format(Locale.US, "[%d, %d, %d, %d, %d]"
                    , mView.mWebView.getLeft()
                    , mView.mWebView.getTop()
                    , mView.mAdContainer.getVisibility()
                    , System.currentTimeMillis()
                    , mVersionCode);
            } else if (action.equals("hideDisplayView") || action.equals("showDisplayView")
                || action.equals("showAd") || action.equals("hideAd")
                || action.equals("onStarted") || action.equals("onStopped")) {
                // hide/show display view
                Runnable run = new Runnable() {
                    @Override public void run() {
                        if (action.equals("hideDisplayView")) {
                            mView.hideDisplayView();
                        } else if (action.equals("showDisplayView")) {
                            mView.showDisplayView();
                        } else if (action.equals("showAd")) {
                            mView.mAdShowing = true;
                            mView.showAd();
                        }  else if (action.equals("hideAd")) {
                            mView.mAdShowing = false;
                            mView.hideAd();
                        } else if (action.equals("onStarted")) {
                            mView.setPlaying(true);
                        } else if (action.equals("onStopped")) {
                            mView.setPlaying(false);
                        }
                        synchronized(this)
                        {
                            this.notify();
                        }
                    }
                };

                synchronized(run) {
                    new Handler(Looper.getMainLooper()).post(run);
                    try {
                        run.wait();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                try {
                    Thread.sleep(100);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return "";
        }

    }

}
