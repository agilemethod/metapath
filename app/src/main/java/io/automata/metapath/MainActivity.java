package io.automata.metapath;

import android.os.Bundle;

import io.automata.common.CommonActivity;

public class MainActivity extends CommonActivity {

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public Class getWindowService() {
        return WindowService.class;
    }

}
