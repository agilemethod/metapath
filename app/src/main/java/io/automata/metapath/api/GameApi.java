package io.automata.metapath.api;

import android.content.Context;

import javax.inject.Inject;

import io.automata.common.api.CommonGameApi;

/**
 * Created by Andy on 2016/1/8.
 */

public class GameApi extends CommonGameApi {

    static {
        System.loadLibrary("metapath");
    }

    @Inject public GameApi(Context context) {
        init(context);
    }

    public native void init(Context context);

    public native void requestBase64(String base64);

    public native String call(String command);

}
