package io.automata.metapath;

import io.automata.common.CommonApp;

/**
 * Created by Andy on 2016/1/14.
 */
public class MainApp extends CommonApp {

    AppComponent mAppComponent;

    @Override public void onCreate() {
        super.onCreate();
        mAppComponent = DaggerAppComponent.builder()
            .appModule(new AppModule(this))
            .build();
    }

    public AppComponent getAppComponent() {
        return mAppComponent;
    }

}
