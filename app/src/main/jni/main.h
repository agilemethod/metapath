//
// Created by Andy on 2016/1/4.
//
#include "jni.h"

#ifndef METAPATH_MAIN_H
#define METAPATH_MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

JNIEXPORT void JNICALL Java_io_automata_metapath_api_GameApi_init(
        JNIEnv *env, jobject obj, jobject thiz);

JNIEXPORT void JNICALL Java_io_automata_metapath_api_GameApi_request(
        JNIEnv *env, jobject obj, jbyteArray buffer);

JNIEXPORT void JNICALL Java_io_automata_metapath_api_GameApi_requestBase64(
        JNIEnv *env, jobject obj, jstring b64);

JNIEXPORT jstring JNICALL Java_io_automata_metapath_api_GameApi_call(
        JNIEnv *env, jobject obj, jstring cmd);

#ifdef __cplusplus
}

#endif
#endif //METAPATH_MAIN_H
