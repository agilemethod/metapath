LOCAL_PATH := $(call my-dir)

#=================================================================#

include $(CLEAR_VARS)

OPENCV_LIB_TYPE:=STATIC
#OPENCV_INSTALL_MODULES:=off
OPENCV_MK_PATH := $(HOME)/AndroidStudio/OpenCV-android-sdk/sdk/native/jni/OpenCV.mk

include $(OPENCV_MK_PATH)

LOCAL_SRC_FILES := \
  src/core/gesture.cpp \
  src/core/image.cpp \
  src/core/matcher.cpp \
  src/human/eye.cpp \
  src/human/finger.cpp \
  src/matapath/gameplayer.cpp \
  src/matapath/layout.cpp \
  src/matapath/match_result.cpp \
  src/matapath/rune_provider.cpp \
  src/matapath/solver.cpp \
  src/matapath/tos_database.cpp \
  src/base64.cpp \
  main.cpp

LOCAL_MODULE := metapath
LOCAL_LDLIBS += -llog -ldl
LOCAL_C_INCLUDES += $(LOCAL_PATH)/include $(LOCAL_PATH)/src

#LOCAL_STATIC_LIBRARIES := opencv_java3
LOCAL_CFLAGS := -std=c++11 -DNDEBUG -O2

include $(BUILD_SHARED_LIBRARY)
