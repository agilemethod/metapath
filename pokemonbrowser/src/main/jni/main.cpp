#include <unistd.h>
#include <cstring>
#include <functional>
#include "main.h"
#include "android/log.h"
#include "base64.h"
#include "pokemonbrowser/gameplayer.h"
#include "matapath/ga_tracker.hpp"

#ifdef NDEBUG
#define LOGD(...)
#else
#define LOGD(...) __android_log_print(ANDROID_LOG_DEBUG  , "libnav",__VA_ARGS__)
#endif

jstring jstring_from_stdstr(JNIEnv * env, const std::string & nativeString) {
	return env->NewStringUTF(nativeString.c_str());
}

std::string stdstr_from_jstring(JNIEnv *env, jstring javaString)
{
	const char *nativeString = env->GetStringUTFChars(javaString, 0);
	std::string result(nativeString, strlen(nativeString));
	env->ReleaseStringUTFChars(javaString, nativeString);
	return result;
}

// =====================================================================

JavaVM * global_jvm = nullptr;

JNIEnv * global_env = nullptr;
jobject global_obj;
PokemonBrowser gameplayer;


namespace JavaRuntime {

JNIEnv * env() { return global_env; }
jobject & obj() { return global_obj; }

}

// =====================================================================

void send_respond_base64(const std::string & raw_string)
{
	if(global_jvm->AttachCurrentThread(&global_env, NULL) != JNI_OK)
	{
		LOGD("%s: AttachCurrentThread() failed", __FUNCTION__);
	}
	std::string base64 = base64_encode(reinterpret_cast<const unsigned char *>(raw_string.c_str()), raw_string.length());
	jstring jbase64 = jstring_from_stdstr(JavaRuntime::env(), base64);
	jclass clazz = JavaRuntime::env()->GetObjectClass(JavaRuntime::obj());
	jmethodID messageMe = JavaRuntime::env()->GetMethodID(clazz, "respondBase64", "(Ljava/lang/String;)V");
	JavaRuntime::env()->CallVoidMethod(JavaRuntime::obj(), messageMe, jbase64);
	LOGD("send_respond_base64 %d", __FUNCTION__);
}

void check_apk(JNIEnv *env, jobject thiz)
{
    jclass cls = env->FindClass("io/automata/pokebrowser/BuildConfig");
    jfieldID debug = env->GetStaticFieldID(cls, "DEBUG", "Z"); //OK
    jboolean is_debug = env->GetStaticBooleanField(cls, debug);
    if (is_debug) return;

    //   context
    jclass native_context = env->GetObjectClass(thiz);

    // context.getPackageManager()
    jmethodID methodID_func = env->GetMethodID(native_context, "getPackageManager", "()Landroid/content/pm/PackageManager;");
    jobject package_manager  = env->CallObjectMethod(thiz,methodID_func);
    jclass pm_clazz = env->GetObjectClass(package_manager);

    //packageManager.getPackageInfo()
    jmethodID methodId_pm = env->GetMethodID(pm_clazz,"getPackageInfo","(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;");

    //context.getPackageName()
    jmethodID methodID_packagename = env->GetMethodID(native_context,"getPackageName","()Ljava/lang/String;");
    jstring name_str = static_cast<jstring>(env->CallObjectMethod(thiz,methodID_packagename));
    jobject package_info = env->CallObjectMethod(package_manager,methodId_pm,name_str,64);
    jclass pi_clazz = env->GetObjectClass(package_info);

    //packageInfo.signatures
    jfieldID fieldID_signatures = env->GetFieldID(pi_clazz,"signatures","[Landroid/content/pm/Signature;");
    jobject signatur = env->GetObjectField(package_info,fieldID_signatures);
    jobjectArray  signatures = reinterpret_cast<jobjectArray>(signatur);

    //signatures[0]
    jobject signature = env->GetObjectArrayElement(signatures,0);
    jclass s_clazz = env->GetObjectClass(signature);

    //signatures[0].toCharString()
    jmethodID methodId_ts = env->GetMethodID(s_clazz,"hashCode","()I");
    jint hash = env->CallIntMethod(signature,methodId_ts);
    //LOGD("Matashift hash %d", hash);
    if (hash != -1148529505)
    {
        int *a = 0;
        *a = 1;
    }
}

JNIEXPORT void JNICALL Java_io_automata_pokebrowser_api_GameApi_init(JNIEnv *env, jobject obj, jobject thiz)
{
	env->GetJavaVM(&global_jvm);

	// setup some global variables...
	if (!global_env) global_env = env;
	if (!global_obj) global_obj = (env->NewGlobalRef(obj));

    check_apk(env, thiz);
    
    //gameplayer.request_administrator();

	// set ga tracker
	GATracker & tracker = GATracker::getInstance();
    tracker.setJavaScreenCaller([](std::string n){
        jstring jname = jstring_from_stdstr(JavaRuntime::env(), n);
        jclass clazz = JavaRuntime::env()->GetObjectClass(JavaRuntime::obj());
        jmethodID messageMe = JavaRuntime::env()->GetMethodID(clazz, "sendTrackerScreen"
            , "(Ljava/lang/String;)V");
        JavaRuntime::env()->CallVoidMethod(JavaRuntime::obj(), messageMe, jname);
    });

    tracker.setJavaEventCaller([](std::string a, std::string c, std::string l){
        jstring ja = jstring_from_stdstr(JavaRuntime::env(), a);
        jstring jc = jstring_from_stdstr(JavaRuntime::env(), c);
        jstring jl = jstring_from_stdstr(JavaRuntime::env(), l);
        jclass clazz = JavaRuntime::env()->GetObjectClass(JavaRuntime::obj());
        jmethodID messageMe = JavaRuntime::env()->GetMethodID(clazz, "sendTrackerEvent"
            , "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V");
        JavaRuntime::env()->CallVoidMethod(JavaRuntime::obj(), messageMe, ja, jc, jl);
    });
}

JNIEXPORT jstring JNICALL Java_io_automata_pokebrowser_api_GameApi_call(
		JNIEnv *env, jobject obj, jstring cmd)
{
	if(global_jvm->AttachCurrentThread(&global_env, NULL) != JNI_OK)
	{
		LOGD("%s: AttachCurrentThread() failed", __FUNCTION__);
	}
	std::string cmd_string = stdstr_from_jstring(JavaRuntime::env(), cmd);

	std::string reuslt = gameplayer.execute(cmd_string);

	LOGD("Java_io_automata_pokebrowser_api_GameApi_call: %s", reuslt.c_str());
	//do something
	jstring result = jstring_from_stdstr(JavaRuntime::env(), reuslt);
	return result;
}