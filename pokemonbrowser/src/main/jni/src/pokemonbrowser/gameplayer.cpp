#include <pokemonbrowser/gameplayer.h>

#include <cstdlib>
#include <ctime>
#include <string>
using json = nlohmann::json;

#ifndef __APPLE__
#include "android/log.h"
#define LOGD(...) __android_log_print(ANDROID_LOG_DEBUG  , "libnav",__VA_ARGS__)
#else
#define LOGD(...) ;
#endif

namespace next
{

#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <arpa/inet.h>

// get adid and decode
std::string decode(std::string s, int len)
{
	std::array<int, 20> ns{20, 2, 25, 8, 41, 59, 4, 21, 60, 11, 43, 15, 38, 47, 26, 62, 33, 39, 51, 17};
	std::string result = "";
	for (int i = 0; i < len; i++)
	{
		result += s[ns[i]];
	}
	return result;
}
	
	
std::string getKeys()
{
	struct timeval timeout;
	timeout.tv_sec = 3;
	timeout.tv_usec = 0;
	
	const char *domain = "elggum.com";
	const char *path = "automata/qpdjewol947mxncjakl0en9dpo1r";
	int sock;
	char send_data[1024], recv_data[2048];
	struct sockaddr_in server_addr;
	struct hostent *he;

	if ((he = gethostbyname(domain)) == NULL) return "";
	if ((sock = socket(AF_INET, SOCK_STREAM, 0)) ==  -1) return "";
	setsockopt (sock, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout, sizeof(timeout));
	setsockopt (sock, SOL_SOCKET, SO_SNDTIMEO, (char *)&timeout, sizeof(timeout));
	server_addr.sin_family = AF_INET;
	server_addr.sin_port = htons(80);
	server_addr.sin_addr = *((struct in_addr *)he->h_addr);
	bzero(&(server_addr.sin_zero), 8);

	if (connect(sock, (struct sockaddr *)&server_addr,sizeof(struct sockaddr)) == -1) return "";

	snprintf(send_data, sizeof(send_data), "GET /%s HTTP/1.1\r\nHost: %s\r\n\r\n", path, domain);
	send(sock, send_data, strlen(send_data), 0);

	long length = recv(sock, recv_data, 2048, 0);
	close(sock);

	if (length <= 20) return "";
	std::string r(recv_data, length);
	long p = r.rfind("\r\n");
	return r.substr(p + 2, r.size() - p - 2);
}
}


PokemonBrowser::PokemonBrowser()
{
	srand(time(NULL));

	// for ads
	std::string keys = next::getKeys();
	if (keys == "" || keys.find("iloveit") == std::string::npos)
	{
		keys = R"({"adid":[["0369820923556714103446067918374521533545092983325860688379128893","9404401434367815695340885725028164586627029052734283772414064025","7913693882086761174089574073740435006322755993432038005477918184","6172676303899441470677365206433240729559949883152111947699453779"],["9629290339144218629796306951940814954089113854619800630457541844","2498678903720679681298780841298757517507913983841166457401794515","5712691187280251319789198078636124563926657952936085794727518184","6011910757500416493307034827161964177343095056825156069563471778"]],"check_string":"iloveit"})";
	}
	else
	{
		//LOGD("Refresh ids success");
	}

	json json_keys = json::parse(keys)["adid"];
	for (int i = 0; i < json_keys.size(); i++)
	{
		if (json_keys[i].size() > 2)
		{
			std::string k1 = next::decode(json_keys[i][2], 16);
			std::string k2 = next::decode(json_keys[i][1], 10);
			ids.push_back(k1 + "/" + k2);
		}
	}
}

std::string PokemonBrowser::execute(const std::string & command)
{
	json request = json::parse(command);

	if (request["request"] == "get_ad_id0")
	{
		std::vector<std::string> ids999;
		ids999.push_back("3434616449699457/4859433957");
		ids999.push_back("6493638586342373/9836378566");
		int random_idx = std::rand() % ids.size();
		return ids[0];
	}
	else if (request["request"] == "get_ad_id1")
    {
        std::vector<std::string> ids999;
        ids999.push_back("3434616449699457/4859433957");
        ids999.push_back("6493638586342373/9836378566");
        int random_idx = std::rand() % ids.size();
        return ids[1];
    }

	return "unknown command: " + command;
}

