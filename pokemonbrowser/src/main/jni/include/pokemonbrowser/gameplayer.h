#ifndef POKEMONBROWSER_GAMEPLAYER_H
#define POKEMONBROWSER_GAMEPLAYER_H

#include <string>
#include <thirdparty/json.hpp>

class GamePlayer
{
public:
	virtual std::string execute(const std::string & command)=0;
};

class PokemonBrowser: public GamePlayer
{
public:
	PokemonBrowser();

public:
	virtual std::string execute(const std::string & command);

private:
	std::vector<std::string> ids;
};

#endif
