package io.automata.pokebrowser;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;


public class IvViewArrayAdapter extends ArrayAdapter<PokemonInfo> {
        private final Context context;
        private ArrayList<PokemonInfo> pokemonList;
        public static final int IV = 0;
        public static final int CP = 1;
        public static final int Number = 2;
        public static final int Name = 3;
        public static final int Time = 4;
        public static final int SortCount = 5;
        private int mSortType = 0;
        private String mSortName = "";

        public IvViewArrayAdapter(Context context, ArrayList<PokemonInfo> pokemonList) {
                super(context, R.layout.pokemon_list_item);
                this.context = context;
                this.pokemonList = pokemonList;
        }

        public void updateDataSet(ArrayList<PokemonInfo> pokemonList) {
                this.pokemonList = pokemonList;
        }

        public void sortBy(int type) {
                mSortType = type;
                switch (type) {
                        case IV:
                                mSortName = "IV";
                                Collections.sort(this.pokemonList, new Comparator<PokemonInfo>() {
                                        @Override
                                        public int compare(PokemonInfo lhs, PokemonInfo rhs) {
                                                return lhs.ivRatio() < rhs.ivRatio() ? 1 : -1;
                                        }
                                });
                                break;
                        case CP:
                                mSortName = "CP";
                                Collections.sort(this.pokemonList, new Comparator<PokemonInfo>() {
                                        @Override
                                        public int compare(PokemonInfo lhs, PokemonInfo rhs) {
                                                return lhs.cp < rhs.cp ? 1 : -1;
                                        }
                                });
                                break;
                        case Number:
                                mSortName = "Number";
                                Collections.sort(this.pokemonList, new Comparator<PokemonInfo>() {
                                        @Override
                                        public int compare(PokemonInfo lhs, PokemonInfo rhs) {
                                                return lhs.pokemonId > rhs.pokemonId ? 1 : -1;
                                        }
                                });
                                break;
                        case Name:
                                mSortName = "Name";
                                Collections.sort(this.pokemonList, new Comparator<PokemonInfo>() {
                                        @Override
                                        public int compare(PokemonInfo lhs, PokemonInfo rhs) {
                                                return lhs.nickName.compareTo(rhs.nickName);
                                        }
                                });
                        case Time:
                                mSortName = "Time";
                                Collections.sort(this.pokemonList, new Comparator<PokemonInfo>() {
                                        @Override
                                        public int compare(PokemonInfo lhs, PokemonInfo rhs) {
                                                return lhs.creationTimeMs < rhs.creationTimeMs ? 1 : -1;
                                        }
                                });
                                break;
                }
        }

        @Override
        public int getCount() {
                return this.pokemonList.size();
        }

        @Override
        public PokemonInfo getItem(int position) {
                return this.pokemonList.get(position);
        }

        @Override
        public long getItemId(int position) {
                return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
                LayoutInflater inflater = (LayoutInflater) context
                                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View ivViewItem = inflater.inflate(R.layout.pokemon_list_item, parent, false);

                ImageView pokeImg = (ImageView) ivViewItem.findViewById(R.id.iv_pokeimg);
                TextView pokeInfo = (TextView) ivViewItem.findViewById(R.id.iv_info);
                TextView pokeAtk = (TextView) ivViewItem.findViewById(R.id.iv_atk);
                TextView pokeDef = (TextView) ivViewItem.findViewById(R.id.iv_def);
                TextView pokeSta = (TextView) ivViewItem.findViewById(R.id.iv_sta);
                TextView pokeRatio = (TextView) ivViewItem.findViewById(R.id.iv_ratio);

                PokemonInfo info = pokemonList.get(position);
                int drawableId = context.getResources().getIdentifier(String.format("ic_pokemon_%03d", info.pokemonId),"drawable", context.getPackageName());
                pokeImg.setImageResource(drawableId);
                pokeInfo.setText(info.nickName + " (LV " + info.lv() + " CP " + info.cp + ")");
                pokeAtk.setText("ATK " + info.attack);
                pokeDef.setText("DEF " + info.defense);
                pokeSta.setText("HP " + info.stamina);
                pokeRatio.setText(String.format("%.1f", info.ivRatio()) + "%");

                //Log.d("getView", "" + position + " " + info.pokemonId + " " + String.format("ic_pokemon_%03d", info.pokemonId));

                return ivViewItem;
        }

        public int sortType() {
                return mSortType;
        }

        public String sortName() {
                return mSortName;
        }
}