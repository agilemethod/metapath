package io.automata.pokebrowser.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import io.automata.pokebrowser.R;

public class JoystickView extends RelativeLayout {

    private View mBase;
    private View mHandle;
    float mBaseRadius;
    float mBaseRadiusSquare;
    float mHandleRadius;
    float mOffset;

    public JoystickView(Context context) {
        this(context, null);
    }

    public JoystickView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public JoystickView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override public void onFinishInflate() {
        super.onFinishInflate();
    }

    @Override public void onMeasure (int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    @Override public void onLayout (boolean changed,
                   int left,
                   int top,
                   int right,
                   int bottom) {
        super.onLayout(changed, left, top, right, bottom);

        if (mBase == null || mHandle == null) {
            mBase = findViewById(R.id.base);
            mHandle = findViewById(R.id.handle);
            mBaseRadius = mBase.getWidth() / 2;
            mBaseRadiusSquare = mBaseRadius * mBaseRadius;
            mHandleRadius = mHandle.getWidth() / 2;
            mOffset = (getWidth() - mBase.getWidth()) / 2;
            setHandleX(mBaseRadius);
            setHandleY(mBaseRadius);
        }
    }

    public void init() {

    }

    public void sendPosition(float x, float y) {
        x -= mOffset;
        y -= mOffset;
        float xFromOrigin = x - mBaseRadius;
        float yFromOrigin = y - mBaseRadius;

        float distanceFromOrigin = xFromOrigin * xFromOrigin + yFromOrigin * yFromOrigin;
        if (distanceFromOrigin > mBaseRadiusSquare) {
            double angle = Math.atan2(yFromOrigin, xFromOrigin);
            x = (float)(Math.cos(angle) * mBaseRadius) + mBaseRadius;
            y = (float)(Math.sin(angle) * mBaseRadius) + mBaseRadius;
        }

        setHandleX(x);
        setHandleY(y);
    }

    private void setHandleX(float x) {
        mHandle.setX(x - mHandleRadius);
    }

    private void setHandleY(float y) {
        mHandle.setY(y - mHandleRadius);
    }

    public double getDgree() {
        double xFromOrigin = getHandleX() - mBaseRadius;
        double yFromOrigin = getHandleY() - mBaseRadius;

        return -Math.toDegrees(Math.atan2(-yFromOrigin, xFromOrigin)) + 90;
    }

    private float getHandleX() {
        return mHandle.getX() + mHandleRadius;
    }

    private float getHandleY() {
        return mHandle.getY() + mHandleRadius;
    }

    public void onFinishInflateAfterInit() {

    }
}
