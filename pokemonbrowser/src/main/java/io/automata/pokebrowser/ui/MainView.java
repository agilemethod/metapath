package io.automata.pokebrowser.ui;

import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.JavascriptInterface;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import io.automata.pokebrowser.MainApp;
import io.automata.pokebrowser.R;
import io.automata.pokebrowser.Settings;
import io.automata.pokebrowser.api.GameApi;

public class MainView extends RelativeLayout {

    static public final int BTN_MAIN = 0;
    static public final int BTN_START = 1;
    static public final int BTN_STOP = 2;
    static public final int BTN_CLOSE = 3;

    public interface OnButtonClick {
        void onButtonClick(int type);
    }

    static public final int GAME_TYPE_MATAPATH = 0;
    static public final int GAME_TYPE_MATASHIFT = 1;

    static private final long AD_CLICK_HIDE_DURING = 6 * 60 * 60 * 1000;
    static private final long AD_UPDATE_TIME = 45 * 1000;
    static private final int AD_SHOWING_TIME = 6;
    static private final long AD_HIDE_TIME = 2 * 60 * 1000;
    static private final long AD_CHECK_TIME = 24 * 60 * 60 * 1000; // 1 day
    static private final long AD_START_CHECK_TIMES = 2;
    static private final String AD_LAST_CLICK_PREF_NAME = "AD_LAST_CLICK_TIME_NAME";
    static private final String AD_PREFERENCE_NAME = "automata_shared_preferences";

    GameApi mCommonGameApi;

    ImageButton mMainBtn;
    LinearLayout mSubMenu;
    ImageButton mStartBtn;
    ImageButton mStopBtn;
    ImageButton mCloseBtn;
    ImageButton mFun1Btn;
    ImageButton mFun2Btn;
    //WebView mWebView;
    AdView mAdView;
    AdView mAdView2;
    RelativeLayout mAdContainer;
    RelativeLayout mAdContainer2;
    TextView mTimerText;
    OnClickListener mOnCloseListener;
    OnClickListener mOnShowListener;
    OnClickListener mOnHideListener;
    OnTouchListener mMainBtnOnTouchListener;
    String mURL1;
    boolean mAdShowing = false;
    boolean mWebViewIsShowing = false;

    private String mAdId;
    private SharedPreferences mPreferences;
    private Timer mHomeTimer;
    private Timer mTimer;
    private int mAdTimes;
    private HandlerThread mHandlerThread = new HandlerThread("HandlerThread");

    private Handler mHandler = new Handler(Looper.getMainLooper());
    private Handler mHandlerBack;
    private MainApp mApp;
    private AdRequest mAdRequest;
    private long mLastUpdateAdTime = System.currentTimeMillis();
    private boolean mIsLoad = false;
    private boolean mIsSize = false;
    private long mStartTime = 0;
    private long mCheckTimes = 0;
    public Settings mSettings;

    public Location mHomeLocation; // blue point
    public Location mFakeLocation; // red marker
    public Location mTargetLocation = new Location("gps"); // blue marker
    public OnButtonClick mOnButtonClick;
    private List<Marker> mPokemons = new ArrayList<>();
    private Map<Long, Marker> mPokemonsGo = new HashMap<>();

    public int mGameType = 0;

    private int speedIdx = 0;
    private int[] speedRes = {R.drawable.speed_1x, R.drawable.speed_2x, R.drawable.speed_4x, R.drawable.speed_6x, R.drawable.speed_8x};
    private int[] speedArray = {1, 2, 4, 6, 8};
    public float basicSpeed = 2.04f; // m/s
    public float speedRatio = 1;

    public UiMapView mUiMapView;

    public GoogleMap googleMap;
    public Marker targetLocMarker;
    public Marker fakeLocMarker;
    private long mTimeClickClose = 0;

    public MainView(Context context) {
        this(context, null);
    }

    public MainView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MainView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    private AdListener mAdListener = new AdListener() {

        @Override public void onAdLeftApplication() {
            super.onAdLeftApplication();
            mPreferences.edit().putLong(AD_LAST_CLICK_PREF_NAME, System.currentTimeMillis()).commit();
            mApp.sendTrackerEvent("Click", "Ad", "Click");
        }

        @Override public void onAdLoaded() {
            mIsLoad = true;
        }

    };

    @Override public void onFinishInflate() {
        super.onFinishInflate();
    }

    public void init(int gameType, GameApi api, String menu) {
        mCommonGameApi = api;
        mURL1 = menu;
        mGameType = gameType;
        onFinishInflateAfterInit();
    }

    public void onFinishInflateAfterInit() {
        mSettings = new Settings(getContext());
        mHandlerThread.start();
        mHandlerBack = new Handler(mHandlerThread.getLooper());

        LocationManager locationManager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) ==
            PackageManager.PERMISSION_GRANTED) {
            if (mFakeLocation == null) {
                mFakeLocation = locationManager.getLastKnownLocation("network");
                mHomeLocation = locationManager.getLastKnownLocation("gps");
            }
            if (mFakeLocation == null) {
                mFakeLocation = locationManager.getLastKnownLocation("gps");
                mHomeLocation = locationManager.getLastKnownLocation("gps");
            }
            locationManager.requestLocationUpdates("network", 1000, 0, new LocationListener() {
                @Override public void onLocationChanged(Location location) {
                    mHomeLocation = location;
                }
                @Override public void onStatusChanged(String provider, int status, Bundle extras) {}
                @Override public void onProviderEnabled(String provider) {}
                @Override public void onProviderDisabled(String provider) {}
            });
        }
        if (mSettings.isStarted()) {
            mFakeLocation = new Location("network");
            mFakeLocation.setTime(System.currentTimeMillis());
            mFakeLocation.setLatitude(mSettings.getLat());
            mFakeLocation.setLongitude(mSettings.getLng());
            mFakeLocation.setAccuracy((float) 23.3);
        }
        if (mFakeLocation == null) {
            mFakeLocation = new Location("network");
            mFakeLocation.setTime(System.currentTimeMillis());
            mFakeLocation.setLatitude(25.047565);
            mFakeLocation.setLongitude(121.516996);
            mFakeLocation.setAccuracy((float) 23.3);
        }
        if (mHomeLocation != null) {
            mTargetLocation.setLatitude(mHomeLocation.getLatitude() + 0.0001);
            mTargetLocation.setLongitude(mHomeLocation.getLongitude() + 0.0001);
        } else {
            mTargetLocation.setLatitude(mFakeLocation.getLatitude() + 0.0001);
            mTargetLocation.setLongitude(mFakeLocation.getLongitude() + 0.0001);
        }


        Log.d("NowLocation", "" + mFakeLocation);

        mApp = (MainApp) getContext().getApplicationContext();
        mApp.sendTrackerScreen(this.getClass().getSimpleName());

        bindView();
        loadWebView();

        mAdId = getAdId();
        mAdView = newAdView(mAdId);
        mAdView2 = newAdView(mAdId);
        mAdContainer.addView(mAdView);
        mAdContainer2.addView(mAdView2);

        ViewGroup.LayoutParams layout_params = mAdContainer.getLayoutParams();
        layout_params.width = LayoutParams.MATCH_PARENT;
        layout_params.height = LayoutParams.WRAP_CONTENT;
        ViewGroup.LayoutParams layout_params2 = mAdContainer2.getLayoutParams();
        layout_params2.width = LayoutParams.MATCH_PARENT;
        layout_params2.height = LayoutParams.WRAP_CONTENT;

        mAdContainer.setLayoutParams(layout_params);
        mAdContainer2.setLayoutParams(layout_params2);

        // init adView
        reloadAd();
        bindAction();

        mPreferences = getContext().getSharedPreferences(AD_PREFERENCE_NAME, 0);
        mStartTime = System.currentTimeMillis();

        updateText();
        // for pokemon browser
//        mStartBtn.setVisibility(VISIBLE); // goBack
//        mStopBtn.setVisibility(VISIBLE); // goForword
    }

    public void mapInit() {
        mUiMapView.mMapView = (MapView) mUiMapView.findViewById(R.id.map);

        mUiMapView.mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override public void onMapReady(GoogleMap map) {
                googleMap = map;
                googleMap.getUiSettings().setMyLocationButtonEnabled(true);
                MapsInitializer.initialize(getContext());
                if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) ==
                    PackageManager.PERMISSION_GRANTED) {
                    googleMap.setMyLocationEnabled(true);
                }

                targetLocMarker = googleMap.addMarker(
                    new MarkerOptions()
                        .title("TargetLocation")
                        .position(new LatLng(mTargetLocation.getLatitude(), mTargetLocation.getLongitude()))
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE))
                        .draggable(true)
                );
                targetLocMarker.setTag("Target");
                googleMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
                    @Override public void onMarkerDragStart(Marker marker) {}

                    @Override public void onMarkerDrag(Marker marker) {}

                    @Override public void onMarkerDragEnd(Marker marker) {
                        if (marker.getTag().equals("Target")) {
                            LatLng latLng = targetLocMarker.getPosition();
                            mTargetLocation.setLatitude(latLng.latitude);
                            mTargetLocation.setLongitude(latLng.longitude);
                            updateText();
                        } else if (marker.getTag().equals("Fake")) {
                            LatLng latLng = fakeLocMarker.getPosition();
                            mFakeLocation.setLatitude(latLng.latitude);
                            mFakeLocation.setLongitude(latLng.longitude);
                            mSettings.update(latLng.latitude, latLng.longitude, true);
                        }
                    }
                });

                googleMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
                    @Override public void onMapLongClick(LatLng latLng) {
                        targetLocMarker.setPosition(latLng);
                        mTargetLocation.setLatitude(latLng.latitude);
                        mTargetLocation.setLongitude(latLng.longitude);
                        updateText();
                    }
                });

                fakeLocMarker = googleMap.addMarker(
                    new MarkerOptions()
                        .title("FakeLocation")
                        .position(new LatLng(mFakeLocation.getLatitude(), mFakeLocation.getLongitude()))
                        .draggable(true)
                );
                fakeLocMarker.setTag("Fake");
                CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(mFakeLocation.getLatitude(), mFakeLocation.getLongitude()), 16);
                googleMap.animateCamera(cameraUpdate);

                googleMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
                    @Override public void onCameraIdle() {
                        final LatLngBounds bounds = googleMap.getProjection().getVisibleRegion().latLngBounds;
                        mHandlerBack.post(new Runnable() {
                            @Override public void run() {
                                updatePokemonInfo(bounds);
                                //updatePokemonInfo2(bounds);
                            }
                        });
                    }
                });
            }
        });

        mUiMapView.setMapWalkButtonClickedListner(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mHomeTimer == null) {
                    startRunning();
                }
            }
        });
    }

    private Location getCurrentLocation() {
        LocationManager locationManager = (LocationManager) getContext().getSystemService(Context.LOCATION_SERVICE);
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) ==
            PackageManager.PERMISSION_GRANTED) {
            return locationManager.getLastKnownLocation("gps");
        }
        return new Location("gps");
    }

    public void updateTargetMarker() {
        if (googleMap != null) {
            mHandler.post(new Runnable() {
                @Override public void run() {
                    targetLocMarker.setPosition(new LatLng(mTargetLocation.getLatitude(), mTargetLocation.getLongitude()));
                }
            });
        }
    }

    public void updateFakeMarker() {
        if (googleMap != null) {
            mHandler.post(new Runnable() {
                @Override public void run() {
                    fakeLocMarker.setPosition(new LatLng(mFakeLocation.getLatitude(), mFakeLocation.getLongitude()));
                }
            });
        }
    }

    private void reloadAd() {
        if (System.currentTimeMillis() - mLastUpdateAdTime > AD_UPDATE_TIME) {
            mAdRequest = new AdRequest.Builder().build();
            mAdView.loadAd(mAdRequest);
            //mAdView2.loadAd(mAdRequest);
            mLastUpdateAdTime = System.currentTimeMillis();
            mApp.sendTrackerScreen("AdView");
        }
    }

    private AdView newAdView(String id) {
        AdView adView = new AdView(getContext());
        adView.setAdSize(AdSize.SMART_BANNER);
        adView.setAdUnitId(id);
        return adView;
    }

    public void startRunning() {
        if(mHomeTimer != null) {
            return;
        }
        mHandler.post(new Runnable() {
            @Override public void run() {
                mFun1Btn.setBackground(getContext().getResources().getDrawable(R.drawable.navigation_enabled));
            }
        });
        mHomeTimer = new Timer();
        mHomeTimer.scheduleAtFixedRate(new TimerTask() {
            @Override public void run() {
                updateText();
                float dis = mFakeLocation.distanceTo(mTargetLocation);
                if (dis > 5) {
                    float needTime = dis / (speedRatio * basicSpeed);
                    double newLat = mFakeLocation.getLatitude()
                        + (mTargetLocation.getLatitude() - mFakeLocation.getLatitude()) / needTime;
                    double newLng = mFakeLocation.getLongitude()
                        + (mTargetLocation.getLongitude() - mFakeLocation.getLongitude()) / needTime;
                    mFakeLocation.setLatitude(newLat);
                    mFakeLocation.setLongitude(newLng);
                    updateFakeMarker();
                    mSettings.update(newLat, newLng, true);
                } else {
                    stopRunning();
                }
            }
        }, 500, 1000);
    }

    public void stopRunning() {
        if (mHomeTimer == null) {
            return;
        }
        mHandler.post(new Runnable() {
            @Override public void run() {
                mFun1Btn.setBackground(getContext().getResources().getDrawable(R.drawable.navigation));
            }
        });
        mHomeTimer.cancel();
        mHomeTimer = null;
    }

    private String getAdId() {
        Log.d("Ad", "ca-app-pub-" + mCommonGameApi.call("{\"request\":\"get_ad_id1\"}"));
        return "ca-app-pub-" + mCommonGameApi.call("{\"request\":\"get_ad_id1\"}");
    }

    private void bindView() {
        mMainBtn = (ImageButton) findViewById(R.id.btn_main);
        mSubMenu = (LinearLayout) findViewById(R.id.sub_menu);
        mStartBtn = (ImageButton) findViewById(R.id.btn_start);
        mStopBtn = (ImageButton) findViewById(R.id.btn_stop);
        mCloseBtn = (ImageButton) findViewById(R.id.btn_close);
        mFun1Btn = (ImageButton) findViewById(R.id.btn_fun1);
        mFun2Btn = (ImageButton) findViewById(R.id.btn_fun2);
        //mWebView = (WebView) findViewById(R.id.browser);
        mAdContainer = (RelativeLayout) findViewById(R.id.adViewContainer);
        mAdContainer2 = (RelativeLayout) findViewById(R.id.adViewContainer2);
        mTimerText = (TextView) findViewById(R.id.timer_text);
    }

    void bindAction() {
        mMainBtn.setOnClickListener(new OnClickListener() {
            @Override public void onClick(View v) {
                toggleDisplayView();
                if (mOnButtonClick != null) {
                    mOnButtonClick.onButtonClick(BTN_MAIN);
                }
            }
        });
        mStartBtn.setOnClickListener(new OnClickListener() {
            @Override public void onClick(View v) {
                if (mOnButtonClick != null) {
                    mOnButtonClick.onButtonClick(BTN_START);
                }
            }
        });
        mStopBtn.setOnClickListener(new OnClickListener() {
            @Override public void onClick(View v) {
                if (mOnButtonClick != null) {
                    mOnButtonClick.onButtonClick(BTN_STOP);
                }
//                String call = String.format("javascript:cmd('%s')", "goNext");
//                if (mWebView.canGoForward()) {
//                    mWebView.goForward();
//                }
            }
        });
        // go home
        mFun1Btn.setOnClickListener(new OnClickListener() {
            @Override public void onClick(View v) {
                if (mHomeTimer == null) {
                    startRunning();
                } else {
                    stopRunning();
                }
                //String call = String.format("javascript:cmd('%s')", "Home");
//                mWebView.loadUrl(call);
            }
        });
        // change speed ratio
        mFun2Btn.setOnClickListener(new OnClickListener() {
            @Override public void onClick(View v) {
                speedIdx++;
                int idx = speedIdx % speedArray.length;
                mFun2Btn.setBackground(getContext().getResources().getDrawable(speedRes[idx]));
                speedRatio = speedArray[idx];
                updateText();
                //Log.d("Change Speed", "Ratio " + speedRatio);
//                loadPlayer();
//                mApp.sendTrackerEvent("Click", "WindowButton", "Reload");
//                String call = String.format("javascript:cmd('%s')", "Reload");
//                mWebView.loadUrl(call);
            }
        });
        mCloseBtn.setOnClickListener(new OnClickListener() {
            @Override public void onClick(View v) {
                if (System.currentTimeMillis() - mTimeClickClose < 5000) {
                    if (mOnCloseListener != null) {
                        mSettings.update(mFakeLocation.getLatitude(), mFakeLocation.getLongitude(), false);
                        mApp.sendTrackerEvent("Click", "WindowButton", "Close");
                        mOnCloseListener.onClick(v);
                    }
                } else {
                    mOnButtonClick.onButtonClick(BTN_CLOSE);
                    mTimeClickClose = System.currentTimeMillis();
                }
            }
        });
        mAdView.setAdListener(mAdListener);
        mAdView2.setAdListener(mAdListener);
//        mWebView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
//            @Override public void onGlobalLayout() {
//                int newVis = mWebView.getVisibility();
//                if (newVis == GONE && mWebViewIsShowing) {
//                    String call = String.format("javascript:cmd('%s')", "DidHide");
//                    mWebView.loadUrl(call);
//                    mWebViewIsShowing = false;
//                } else if (newVis == VISIBLE && !mWebViewIsShowing) {
//                    String call = String.format("javascript:cmd('%s')", "DidShow");
//                    mWebView.loadUrl(call);
//                    mWebViewIsShowing = true;
//                }
//            }
//        });
    }

    public void updateText() {
        mHandler.post(new Runnable() {
            @Override public void run() {
                mTimerText.setText(String.format(Locale.US, "%dm", (int) mTargetLocation.distanceTo(mFakeLocation)));
            }
        });

    }

    private void updatePokemonInfo2(LatLngBounds bounds) {
        if (googleMap == null) {
            return;
        }
        LatLng maxLatLng = bounds.northeast;
        LatLng minLatLng = bounds.southwest;
        if (maxLatLng.latitude - minLatLng.latitude > 0.02 || maxLatLng.longitude - maxLatLng.longitude > 0.02) {
            return;
        }
//'Cookie: _ga=GA1; _gat=1; poke5566=lat0=25.077025045870553&lng0=121.40237892903133&lat1=25.024150312870546&lng1=121.35148133076473'
//'Accept-Encoding: gzip, deflate, sdch, br'
//'Accept-Language: zh-TW,zh;q=0.8,en-US;q=0.6,en;q=0.4,ko;q=0.2,zh-CN;q=0.2'
//'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36'
//'X-Requested-With: XMLHttpRequest'
        try {
            String reqUrl = String.format(Locale.US, "https://poke5566.com/pokemons?lat0=%f&lng0=%f&lat1=%f&lng1=%f",
                maxLatLng.latitude, maxLatLng.longitude, minLatLng.latitude, minLatLng.longitude);
            URL url = new URL(reqUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Cookie", String.format(Locale.US, "_ga=GA1; _gat=1; poke5566=lat0=%f&lng0=%f&lat1=%f&lng1=%f",
                maxLatLng.latitude, maxLatLng.longitude, minLatLng.latitude, minLatLng.longitude));
            conn.setRequestProperty("Accept-Encoding", "gzip, deflate, sdch, br");
            conn.setRequestProperty("Accept-Language", "zh-TW,zh;q=0.8,en-US;q=0.6,en;q=0.4,ko;q=0.2,zh-CN;q=0.2");
            conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36");
            conn.setRequestProperty("X-Requested-With", "XMLHttpRequest");
            conn.setUseCaches(false);
            conn.setDoInput(true);
            conn.setDoOutput(true);
            BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
            String str;
            String jsonText = "";
            while ((str = in.readLine()) != null) {
                jsonText += str;
            }
            Log.d("Get Pokemon Info 5566", jsonText);
            in.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void updatePokemonInfo(LatLngBounds bounds) {
        Log.d("Get Pokemon", "updatePokemonInfo");
        if (googleMap == null) {
            return;
        }
        LatLng maxLatLng = bounds.northeast;
        LatLng minLatLng = bounds.southwest;

        if (maxLatLng.latitude - minLatLng.latitude > 0.02 || maxLatLng.longitude - maxLatLng.longitude > 0.02) {
            return;
        }
        //Log.d("Get Pokemon Info", "start to get");

        //https://www.pokeradar.io/api/v1/submissions?deviceId=3rdsfjenj&minLatitude=25.01707599728341&maxLatitude=25.024834128298057&minLongitude=121.41937494277953&maxLongitude=121.43825769424438&pokemonId=0
        String reqUrl = String.format(Locale.US,
            "https://www.pokeradar.io/api/v1/submissions?deviceId=%s&minLatitude=%f&maxLatitude=%f&minLongitude=%f&maxLongitude=%f&pokemonId=0",
            Build.SERIAL,minLatLng.latitude, maxLatLng.latitude, minLatLng.longitude, maxLatLng.longitude);
        //Log.d("Get Pokemon Info", "start to get url " + reqUrl);
        try {
            URL url = new URL(reqUrl);
            // Read all the text returned by the server
            BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
            String str;
            String jsonText = "";
            while ((str = in.readLine()) != null) {
                jsonText += str;
            }
            //Log.d("Get Pokemon Info", jsonText);
            in.close();
            final String text = jsonText;
            mHandler.post(new Runnable() {
                @Override public void run() {
                    showOnMapPokemon(text);
                }
            });
        } catch (MalformedURLException e) {
        } catch (IOException e) {}
    }

    private void showOnMapPokemon(String text) {
        if (googleMap == null) {
            return;
        }
        for (int i = 0; i < mPokemons.size(); i++) {
            mPokemons.get(i).remove();
        }
        mPokemons.clear();
        SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss", Locale.US);
        try {
            //{"data":[{"id":"1473342708-129-25.0224116378-121.4267344648","created":1473342708,"downvotes":0,"upvotes":1,"latitude":25.0224116378,"longitude":121.4267344648,"pokemonId":129,"trainerName":"(Poke Radar Prediction)","userId":"13661365","deviceId":"80sxy0vumg2h5hhv8hgc0axt9jr29al7"},{"id":"1473342758-116-25.017156-121.42789","created":1473342758,"downvotes":0,"upvotes":1,"latitude":25.017156,"longitude":121.42789,"pokemonId":116,"trainerName":"(Poke Radar Prediction)","userId":"13661365","deviceId":"80sxy0vumg2h5hhv8hgc0axt9jr29al7"},{"id":"1473343287-127-25.017156-121.42789","created":1473343287,"downvotes":0,"upvotes":1,"latitude":25.017156,"longitude":121.42789,"pokemonId":127,"trainerName":"(Poke Radar Prediction)","userId":"13661365","deviceId":"80sxy0vumg2h5hhv8hgc0axt9jr29al7"},{"id":"1473343324-60-25.024317-121.423825","created":1473343324,"downvotes":0,"upvotes":1,"latitude":25.024317,"longitude":121.423825,"pokemonId":60,"trainerName":"(Poke Radar Prediction)","userId":"13661365","deviceId":"80sxy0vumg2h5hhv8hgc0axt9jr29al7"},{"id":"1473343289-13-25.0252711873-121.4279845269","created":1473343289,"downvotes":0,"upvotes":1,"latitude":25.0252711873,"longitude":121.4279845269,"pokemonId":13,"trainerName":"(Poke Radar Prediction)","userId":"13661365","deviceId":"80sxy0vumg2h5hhv8hgc0axt9jr29al7"},{"id":"1473342676-61-25.0178058224-121.4276273677","created":1473342676,"downvotes":0,"upvotes":1,"latitude":25.0178058224,"longitude":121.4276273677,"pokemonId":61,"trainerName":"(Poke Radar Prediction)","userId":"13661365","deviceId":"80sxy0vumg2h5hhv8hgc0axt9jr29al7"},{"id":"1473342711-16-25.025272-121.430417","created":1473342711,"downvotes":0,"upvotes":1,"latitude":25.025272,"longitude":121.430417,"pokemonId":16,"trainerName":"(Poke Radar Prediction)","userId":"13661365","deviceId":"80sxy0vumg2h5hhv8hgc0axt9jr29al7"},{"id":"1473342648-93-25.018863-121.426284","created":1473342648,"downvotes":0,"upvotes":1,"latitude":25.018863,"longitude":121.426284,"pokemonId":93,"trainerName":"(Poke Radar Prediction)","userId":"13661365","deviceId":"80sxy0vumg2h5hhv8hgc0axt9jr29al7"},{"id":"1473342693-10-25.0172035348-121.4280738166","created":1473342693,"downvotes":0,"upvotes":1,"latitude":25.0172035348,"longitude":121.4280738166,"pokemonId":10,"trainerName":"(Poke Radar Prediction)","userId":"13661365","deviceId":"80sxy0vumg2h5hhv8hgc0axt9jr29al7"},{"id":"1473342743-129-25.0252864584-121.4307524736","created":1473342743,"downvotes":0,"upvotes":1,"latitude":25.0252864584,"longitude":121.4307524736,"pokemonId":129,"trainerName":"(Poke Radar Prediction)","userId":"13661365","deviceId":"80sxy0vumg2h5hhv8hgc0axt9jr29al7"},{"id":"1473343264-54-25.025324-121.430807","created":1473343264,"downvotes":0,"upvotes":1,"latitude":25.025324,"longitude":121.430807,"pokemonId":54,"trainerName":"(Poke Radar Prediction)","userId":"13661365","deviceId":"80sxy0vumg2h5hhv8hgc0axt9jr29al7"},{"id":"1473343242-147-25.0222875473-121.4265558834","created":1473343242,"downvotes":0,"upvotes":1,"latitude":25.0222875473,"longitude":121.4265558834,"pokemonId":147,"trainerName":"(Poke Radar Prediction)","userId":"13661365","deviceId":"80sxy0vumg2h5hhv8hgc0axt9jr29al7"},{"id":"1473342621-16-25.019524944-121.4287881312","created":1473342621,"downvotes":0,"upvotes":1,"latitude":25.019524944,"longitude":121.4287881312,"pokemonId":16,"trainerName":"(Poke Radar Prediction)","userId":"13661365","deviceId":"80sxy0vumg2h5hhv8hgc0axt9jr29al7"},{"id":"1473342721-1-25.017474-121.427429","created":1473342721,"downvotes":0,"upvotes":1,"latitude":25.017474,"longitude":121.427429,"pokemonId":1,"trainerName":"(Poke Radar Prediction)","userId":"13661365","deviceId":"80sxy0vumg2h5hhv8hgc0axt9jr29al7"},{"id":"1473342828-129-25.025324-121.430807","created":1473342828,"downvotes":0,"upvotes":1,"latitude":25.025324,"longitude":121.430807,"pokemonId":129,"trainerName":"(Poke Radar Prediction)","userId":"13661365","deviceId":"80sxy0vumg2h5hhv8hgc0axt9jr29al7"},{"id":"1473342827-120-25.021587-121.427477","created":1473342827,"downvotes":0,"upvotes":1,"latitude":25.021587,"longitude":121.427477,"pokemonId":120,"trainerName":"(Poke Radar Prediction)","userId":"13661365","deviceId":"80sxy0vumg2h5hhv8hgc0axt9jr29al7"},{"id":"1473342857-127-25.0243865187-121.4243235925","created":1473342857,"downvotes":0,"upvotes":1,"latitude":25.0243865187,"longitude":121.4243235925,"pokemonId":127,"trainerName":"(Poke Radar Prediction)","userId":"13661365","deviceId":"80sxy0vumg2h5hhv8hgc0axt9jr29al7"},{"id":"1473343279-7-25.0176824057-121.4270916268","created":1473343279,"downvotes":0,"upvotes":1,"latitude":25.0176824057,"longitude":121.4270916268,"pokemonId":7,"trainerName":"(Poke Radar Prediction)","userId":"13661365","deviceId":"80sxy0vumg2h5hhv8hgc0axt9jr29al7"},{"id":"1473343258-13-25.018323-121.425304","created":1473343258,"downvotes":0,"upvotes":1,"latitude":25.018323,"longitude":121.425304,"pokemonId":13,"trainerName":"(Poke Radar Prediction)","userId":"13661365","deviceId":"80sxy0vumg2h5hhv8hgc0axt9jr29al7"},{"id":"1473343358-19-25.0252258372-121.429948883","created":1473343358,"downvotes":0,"upvotes":1,"latitude":25.0252258372,"longitude":121.429948883,"pokemonId":19,"trainerName":"(Poke Radar Prediction)","userId":"13661365","deviceId":"80sxy0vumg2h5hhv8hgc0axt9jr29al7"},{"id":"1473343312-129-25.016735-121.42553","created":1473343312,"downvotes":0,"upvotes":1,"latitude":25.016735,"longitude":121.42553,"pokemonId":129,"trainerName":"(Poke Radar Prediction)","userId":"13661365","deviceId":"80sxy0vumg2h5hhv8hgc0axt9jr29al7"}],"success":true,"errors":[]}
            JSONObject jObject = new JSONObject(text);
            JSONArray jArray = jObject.getJSONArray("data");
            for (int i = 0; i < jArray.length(); i++) {
                JSONObject obj = jArray.getJSONObject(i);
                double lat = obj.getDouble("latitude");
                double lng = obj.getDouble("longitude");
                int pid = obj.getInt("pokemonId");
                int uid = obj.getInt("userId");
                int downvotes = obj.getInt("downvotes");
                int upvotes = obj.getInt("upvotes");
                float score = (float) upvotes * 100 / (upvotes + downvotes);
                String name = obj.getString("trainerName");
                String deviceId = obj.getString("deviceId");
                long created = obj.getLong("created");
                long hiddenTime = (created + 15 * 60) * 1000;
                if (!name.equals("(Poke Radar Prediction)")) {
                    continue;
                }
                if (!deviceId.equals("80sxy0vumg2h5hhv8hgc0axt9jr29al7")) {
                    continue;
                }
                if (score < 80) {
                    continue;
                }
                String resName = String.format(Locale.US, "ic_pokemon_%03d", pid);
                int r = getResId(resName, R.drawable.class);
                if (r == -1) {
                    continue;
                }
                BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(r);
                Marker p = googleMap.addMarker(
                    new MarkerOptions()
                        .title("Third " + df.format(hiddenTime))
                        .position(new LatLng(lat,lng))
                        .icon(icon)
                        .anchor(0.5f, 0.5f)
                );
                mPokemons.add(p);
            }
        } catch (JSONException e) {}
    }

    public void newNearPokemon(String json) {
        if (googleMap == null) {
            return;
        }
        try {
            JSONObject jobj = new JSONObject(json);
            final long encounterId = jobj.getLong("encounter_id");
            final int pokemon_id = jobj.getInt("pokemon_id");
            final double lat = jobj.getDouble("latitude");
            final double lng = jobj.getDouble("longitude");
            final long hiddenTime = jobj.getLong("hidden_time");
            final long remainingTime = hiddenTime - System.currentTimeMillis();
            final String resName = String.format(Locale.US, "ic_pokemon_%03d", pokemon_id);
            final int r = getResId(resName, R.drawable.class);
            if (r == -1) {
                return;
            }
            if (remainingTime < 0) {
                return;
            }
            if (mPokemonsGo.containsKey(encounterId)) {
                return;
            }
            mHandler.post(new Runnable() {
                @Override public void run() {
                    SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss", Locale.US);
                    BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(r);
                    Marker p = googleMap.addMarker(
                        new MarkerOptions()
                            .title("Real " + df.format(hiddenTime))
                            .position(new LatLng(lat,lng))
                            .icon(icon)
                            .anchor(0.5f, 0.5f)
                    );
                    mPokemonsGo.put(encounterId, p);
                    mHandler.postDelayed(new Runnable() {
                        @Override public void run() {
                            Marker m = mPokemonsGo.get(encounterId);
                            if (m != null) {
                                m.remove();
                            }
                            mPokemonsGo.remove(encounterId);
                        }
                    }, remainingTime);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static int getResId(String resName, Class<?> c) {
        try {
            Field idField = c.getDeclaredField(resName);
            return idField.getInt(idField);
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    void setPlaying(Boolean playing) {
        // do nothing in pokemon browser
    }

    public boolean isShowing() {
        return mSubMenu.getVisibility() == VISIBLE;
    }

    public void toggleDisplayView() {
        if (mSubMenu.getVisibility() == VISIBLE) {
            hideDisplayView();
        } else {
            showDisplayView();
        }
    }

    void hideDisplayView() {
        mSubMenu.setVisibility(GONE);
//        mWebView.setVisibility(GONE);
        hideAd();
        mAdContainer.setVisibility(GONE);
        if (mOnHideListener != null) {
            mOnHideListener.onClick(null);
        }
    }

    void showDisplayView() {
        if (mOnShowListener != null) {
            mOnShowListener.onClick(null);
        }
        mSubMenu.setVisibility(VISIBLE);
        mAdContainer.setVisibility(VISIBLE);
//        mWebView.setVisibility(VISIBLE);
        //checkAndShowAd();
        //showAd();
        reloadAd();
    }

    void checkAndShowAd() {
        // pokemon browser always show Ad1
        mAdContainer.setVisibility(VISIBLE);
    }

    void hideAd() {
        //mAdContainer2.setVisibility(GONE);
    }

    void showAd() {
        //mApp.sendTrackerEvent("Visible", "Ad", "ShowAd2");
        //mAdContainer2.setVisibility(VISIBLE);
    }

    void loadPlayer() {
        ConnectivityManager cm = (ConnectivityManager) getContext()
            .getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo i = cm.getActiveNetworkInfo();
        if ((i == null) || (!i.isConnected())) {

        } else {
//            mWebView.loadUrl(mURL1);
        }
    }

    void loadWebView() {
//        WebSettings websettings = mWebView.getSettings();
//        websettings.setSupportZoom(true);
//        websettings.setBuiltInZoomControls(true);
//        websettings.setJavaScriptEnabled(true);
//        websettings.setDomStorageEnabled(true);
//        websettings.setBuiltInZoomControls(false);
//        websettings.setSupportZoom(false);
//        mWebView.setWebViewClient(new WebViewClient());
//        loadPlayer();
//        mWebView.addJavascriptInterface(new JSHandler(this), "Bridge");
//        mWebView.setWebViewClient(new WebViewClient() {
//            @Override
//            public void onPageFinished(WebView view, String url) {
//                super.onPageFinished(view, url);
//                view.setBackgroundColor(Color.TRANSPARENT);
//                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
//                    view.setLayerType(View.LAYER_TYPE_HARDWARE, null);
//                } else {
//                    view.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
//                }
//            }
//        });
//        mWebView.setBackgroundColor(Color.TRANSPARENT);
    }

    public void setOnShowListener(OnClickListener l) {
        mOnShowListener = l;
    }

    public void setMainBtnOnTouchListener(OnTouchListener l) {
        mMainBtnOnTouchListener = l;
        mMainBtn.setOnTouchListener(mMainBtnOnTouchListener);
    }

    public void setOnHideListener(OnClickListener l) {
        mOnHideListener = l;
    }
    public void setOnCloseListener(OnClickListener l) {
        mOnCloseListener = l;
    }

    private static class JSHandler {

        MainView mView;
        Handler mHandler;
        String mMessage = "";
        int mVersionCode = 0;

        GameApi.RespondListener mlistener = new GameApi.RespondListener() {
            @Override public void sendTrackerScreen(final String n) {
                mView.mApp.sendTrackerScreen(n);
            }

            @Override public void sendTrackerEvent(final String a, final String c, final String l) {
                mView.mApp.sendTrackerEvent(a, c, l);
            }
        };

        public JSHandler(MainView view) {
            mView = view;
            mView.mCommonGameApi.listener = mlistener;
            mView = view;
            mHandler = new Handler(Looper.getMainLooper());
        }

        @JavascriptInterface
        public String call(final String cmd) {
            return mView.mCommonGameApi.call(cmd);
        }

        @JavascriptInterface
        public String callApp(final String action) {
            Log.d("CallApp", action);
            if (action.equals("getWidthHeight")) {
                Context c = mView.getContext();
                WindowManager w = (WindowManager) c.getSystemService(Context.WINDOW_SERVICE);
                Point size = new Point();
                w.getDefaultDisplay().getSize(size);
                return String.format("[%d, %d]", size.x, size.y);
            } else if (action.equals("getWebViewXY")) {
//                return String.format("[%d, %d]", mView.mWebView.getLeft(), mView.mWebView.getTop());
            } else if (action.contains("trackScreen")) {
                mView.mApp.sendTrackerScreen(action);
            } else if (action.contains("trackEvent")) {
                mView.mApp.sendTrackerEvent("event", "webviewEvent", action);
            } else if (action.contains("getLocation")) {
                return String.format("[%f, %f]", mView.mFakeLocation.getLatitude(), mView.mFakeLocation.getLongitude());
            } else if (action.contains("setLocation")) {
                // setLocation,22.33333,121.22222
                String[] split = action.split(",");
                if (split.length == 3) {
                    Double lat = Double.parseDouble(split[1]);
                    Double lng = Double.parseDouble(split[2]);
                    mView.mFakeLocation.setLatitude(lat);
                    mView.mFakeLocation.setLongitude(lng);
                    mView.mSettings.update(lat, lng, true);
                    Log.d("CallApp", "Update location " + lat + " " +lng);
                }
            } else if (action.equals("getVersionName")) {
                try {
                    PackageInfo pInfo = mView.getContext().getPackageManager()
                        .getPackageInfo(mView.getContext().getPackageName(), 0);
                    return pInfo.versionName;
                } catch (Exception e) {
                    return "";
                }
            } else if (action.equals("getVersionCode")) {
                try {
                    PackageInfo pInfo = mView.getContext().getPackageManager()
                        .getPackageInfo(mView.getContext().getPackageName(), 0);
                    mVersionCode = pInfo.versionCode;
                    return Integer.toString(pInfo.versionCode);
                } catch (Exception e) {
                    return "";
                }
            } else if (action.equals("getExternalPath")) {
                return Environment.getExternalStorageDirectory().getAbsolutePath();
            } else if (action.equals("getUserFilePath")) {
                //File f = mView.getContext().getExternalFilesDir(null);
                File f = mView.getContext().getFilesDir();
                if (f != null) {
                    return f.getAbsolutePath();
                }
                return "";
            } else if (action.contains("getUiStatus")) {
//                return String.format(Locale.US, "[%d, %d, %d, %d, %d]"
//                    , mView.mWebView.getLeft()
//                    , mView.mWebView.getTop()
//                    , mView.mAdContainer.getVisibility()
//                    , System.currentTimeMillis()
//                    , mVersionCode);
            } else if (action.equals("hideDisplayView") || action.equals("showDisplayView")
                || action.equals("showAd") || action.equals("hideAd")
                || action.equals("onStarted") || action.equals("onStopped")) {
                // hide/show display view
                Runnable run = new Runnable() {
                    @Override public void run() {
                        if (action.equals("hideDisplayView")) {
                            mView.hideDisplayView();
                        } else if (action.equals("showDisplayView")) {
                            mView.showDisplayView();
                        } else if (action.equals("showAd")) {
                            mView.mAdShowing = true;
                            mView.showAd();
                        }  else if (action.equals("hideAd")) {
                            mView.mAdShowing = false;
                            mView.hideAd();
                        } else if (action.equals("onStarted")) {
                            mView.setPlaying(true);
                        } else if (action.equals("onStopped")) {
                            mView.setPlaying(false);
                        }
                        synchronized(this)
                        {
                            this.notify();
                        }
                    }
                };

                synchronized(run) {
                    new Handler(Looper.getMainLooper()).post(run);
                    try {
                        run.wait();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                try {
                    Thread.sleep(100);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return "";
        }

    }

}
