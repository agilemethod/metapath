package io.automata.pokebrowser.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

/**
 * Created by Andy on 2016/8/24.
 */
public class RockerView extends RelativeLayout {

    public RockerView(Context context) {
        this(context, null);
    }

    public RockerView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RockerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override public void onFinishInflate() {
        super.onFinishInflate();
    }

    public void init() {

    }

    public void onFinishInflateAfterInit() {

    }
}
