package io.automata.pokebrowser.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

/**
 * Created by Andy on 2016/9/8.
 */
public class NotificationView extends RelativeLayout {

    public NotificationView(Context context) {
        this(context, null);
    }

    public NotificationView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public NotificationView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override public void onFinishInflate() {
        super.onFinishInflate();
    }

    public void init() {}
}
