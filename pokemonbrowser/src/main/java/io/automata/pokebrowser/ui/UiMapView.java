package io.automata.pokebrowser.ui;

import android.content.Context;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.aeonlucid.pogoprotos.Enums;
import com.google.android.gms.maps.MapView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import io.automata.pokebrowser.IvViewArrayAdapter;
import io.automata.pokebrowser.MainApp;
import io.automata.pokebrowser.PokemonInfo;
import io.automata.pokebrowser.R;

/**
 * Created by Andy on 2016/8/30.
 */

public class UiMapView extends RelativeLayout {

    public static final int SHOW_TYPE_NONE = 0;
    public static final int SHOW_TYPE_MAP = 1;
    public static final int SHOW_TYPE_POKE = 2;

    public RelativeLayout mMapContainer;
    public RelativeLayout mPokemonContainer;
    public MapView mMapView;
    public int mShowingType = SHOW_TYPE_NONE;
    private ListView mPokemonListView;
    private Button mPokemonSortButton;
    private IvViewArrayAdapter mPokemonListViewAdapter;
    private Button mMapWalkButton;
    private OnClickListener mMapWalkButtonClickedListner;
    private MainApp mApp;

    public UiMapView(Context context) {
        this(context, null);
    }

    public UiMapView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public UiMapView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override public void onFinishInflate() {
        super.onFinishInflate();
    }

    public void init() {
        mMapContainer = (RelativeLayout) this.findViewById(R.id.map_container);
        mMapWalkButton = (Button) mMapContainer.findViewById(R.id.map_walk_button);
        mPokemonContainer = (RelativeLayout) this.findViewById(R.id.pokemon_container);
        mMapView = (MapView) this.findViewById(R.id.map);
        initPokemonList();
        mMapView.onCreate(new Bundle());
        mApp = (MainApp) getContext().getApplicationContext();
    }

    public void setMapWalkButtonClickedListner(OnClickListener listener) {
        mMapWalkButtonClickedListner = listener;
        mMapWalkButton.setOnClickListener(mMapWalkButtonClickedListner);
    }

    public void updatePokemonList(String pokemonText) {
        ArrayList<PokemonInfo> pokemonList = null;
        try {
            pokemonList = parsePokemonJson(pokemonText);
            TextView tv = (TextView) this.findViewById(R.id.pokemon_help);
            if (pokemonList.size() == 0) {
                tv.setVisibility(VISIBLE);
                mPokemonListView.setVisibility(GONE);
            } else {
                tv.setVisibility(GONE);
                mPokemonListView.setVisibility(VISIBLE);
                mPokemonListViewAdapter.updateDataSet(pokemonList);
                mPokemonListViewAdapter.notifyDataSetChanged();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void initPokemonList() {
        mPokemonListView = (ListView) findViewById(R.id.pokemon_list);
        mPokemonListViewAdapter = new IvViewArrayAdapter(getContext(), new ArrayList<PokemonInfo>());
        mPokemonListView.setAdapter(mPokemonListViewAdapter);
        mPokemonListViewAdapter.sortBy(IvViewArrayAdapter.IV);
        mPokemonListViewAdapter.notifyDataSetChanged();

        mPokemonSortButton = (Button) mPokemonContainer.findViewById(R.id.pokemon_sort_button);
        mPokemonSortButton.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v) {
                int sortType = mPokemonListViewAdapter.sortType();
                mPokemonListViewAdapter.sortBy((sortType + 1) % IvViewArrayAdapter.SortCount);
                mPokemonListViewAdapter.notifyDataSetChanged();
                mPokemonSortButton.setText("sort By " + mPokemonListViewAdapter.sortName());
            }
        });

        //String pokemonText = "{\"pokemons\":[{\"id\":-8410605429510914304,\"pokemon_id\":19,\"cp\":55,\"stamina_max\":19,\"individual_attack\":8,\"individual_defense\":10,\"individual_stamina\":15,\"creation_time_ms\":1472271841350,\"move_1\":219,\"move_2\":129},{\"id\":3821392690307545409,\"pokemon_id\":19,\"cp\":10,\"stamina_max\":10,\"individual_attack\":9,\"individual_defense\":1,\"individual_stamina\":15,\"creation_time_ms\":1472048482921,\"move_1\":219,\"move_2\":131},{\"id\":-4701121864914408246,\"pokemon_id\":27,\"cp\":91,\"stamina_max\":33,\"individual_attack\":5,\"individual_defense\":1,\"individual_stamina\":14,\"creation_time_ms\":1472280238802,\"move_1\":216,\"move_2\":63},{\"id\":1198768860000272312,\"pokemon_id\":16,\"cp\":25,\"stamina_max\":15,\"individual_attack\":4,\"individual_defense\":2,\"individual_stamina\":13,\"creation_time_ms\":1472046548352,\"move_1\":219,\"move_2\":80},{\"id\":-1381737712633901071,\"pokemon_id\":7,\"cp\":134,\"stamina_max\":30,\"individual_attack\":0,\"individual_defense\":3,\"individual_stamina\":6,\"creation_time_ms\":1472280849307,\"move_1\":221,\"move_2\":105},{\"id\":6485406039746797699,\"pokemon_id\":8,\"cp\":141,\"stamina_max\":30,\"individual_attack\":0,\"individual_defense\":11,\"individual_stamina\":3,\"creation_time_ms\":1472060783676,\"move_1\":230,\"move_2\":107},{\"id\":2783174068814665930,\"pokemon_id\":129,\"cp\":37,\"stamina_max\":15,\"individual_attack\":11,\"individual_defense\":11,\"individual_stamina\":9,\"creation_time_ms\":1472281166707,\"move_1\":231,\"move_2\":133}]}";
        String pokemonText = "{\"pokemons\":[]}";
        updatePokemonList(pokemonText);
    }

    private ArrayList<PokemonInfo> parsePokemonJson(String text) throws JSONException {
        ArrayList<PokemonInfo> result = new ArrayList<>();

        JSONObject jPokemonPackage = new JSONObject(text);
        JSONArray jPokemonArray = jPokemonPackage.getJSONArray("pokemons");

        for (int i = 0; i < jPokemonArray.length(); i++) {
            JSONObject jInfo = jPokemonArray.getJSONObject(i);
            PokemonInfo info = new PokemonInfo();
            info.pokemonId = jInfo.getInt("pokemon_id");
            info.cp = jInfo.getInt("cp");
            info.staminaMax = jInfo.getInt("stamina_max");
            info.attack = jInfo.getInt("individual_attack");
            info.defense = jInfo.getInt("individual_defense");
            info.stamina = jInfo.getInt("individual_stamina");
            info.creationTimeMs = jInfo.getLong("creation_time_ms");
            info.move1 = jInfo.getInt("move_1");
            info.move2 = jInfo.getInt("move_2");
            if (jInfo.has("nickname")) {
                info.nickName = jInfo.getString("nickname");
                Enums.PokemonId pid = Enums.PokemonId.forNumber(info.pokemonId);
                if (pid != null) {
                    info.nickName = pid.name();
                }
            }
            if (info.nickName == null) {
                info.nickName = "";
            }
            if (jInfo.has("cp_multiplier")) {
                double cp_mulitplier = jInfo.getDouble("cp_multiplier");
                info.setLV(cp_mulitplier);
            } else {
                info.setLV(0);
            }
            result.add(info);
        }

        return result;
    }

    public boolean toggleMap() {
        switch (mShowingType) {
        case SHOW_TYPE_NONE:
            showMap();
            return true;
        case SHOW_TYPE_MAP:
            hide();
            return false;
        case SHOW_TYPE_POKE:
            showMap();
            return true;
        }
        return false;
    }

    public boolean togglePoke() {
        switch (mShowingType) {
        case SHOW_TYPE_NONE:
            showPoke();
            return true;
        case SHOW_TYPE_MAP:
            showPoke();
            return true;
        case SHOW_TYPE_POKE:
            hide();
            return false;
        }
        return false;
    }

    public void showMap() {
        mApp.sendTrackerScreen("MapView");
        mPokemonContainer.setVisibility(GONE);
        mMapContainer.setVisibility(VISIBLE);
        mMapView.setVisibility(VISIBLE);
        mMapView.onResume();
        mShowingType = SHOW_TYPE_MAP;
    }

    public void hide() {
        mMapContainer.setVisibility(GONE);
        mPokemonContainer.setVisibility(GONE);
        mMapView.setVisibility(INVISIBLE);
        mMapView.onPause();
        mShowingType = SHOW_TYPE_NONE;
    }

    public void showPoke() {
        mApp.sendTrackerScreen("IvView");
        mMapContainer.setVisibility(GONE);
        mPokemonContainer.setVisibility(VISIBLE);
        mMapView.setVisibility(GONE);
        mMapView.onPause();
        mShowingType = SHOW_TYPE_POKE;
    }

    public void onFinishInflateAfterInit() {

    }


}
