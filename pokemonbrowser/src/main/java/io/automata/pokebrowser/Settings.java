package io.automata.pokebrowser;

import android.content.Context;
import android.content.SharedPreferences;

import de.robv.android.xposed.XSharedPreferences;

/**
 * Created by Andy on 2016/8/22.
 */
public class Settings {

    private static double lat = 25.021172; // 22.318344;
    private static double lng = 121.427062; // 114.168655;
    private static boolean start = false;
    private Context context = null;
    private XSharedPreferences xSharedPreferences = null;
    private SharedPreferences sharedPreferences = null;

    public Settings() {
        xSharedPreferences = new XSharedPreferences("io.automata.pokegorunner", "gps");
    }

    public Settings(Context context) {
        sharedPreferences = context.getSharedPreferences("gps", Context.MODE_WORLD_READABLE);
        this.context = context;
    }

    public double getLat() {
        if (sharedPreferences != null)
            return sharedPreferences.getFloat("latitude", (float) 25.021172);
        else if (xSharedPreferences != null)
            return xSharedPreferences.getFloat("latitude", (float) 25.021172);
        return lat;
    }

    public double getLng() {
        if (sharedPreferences != null)
            return sharedPreferences.getFloat("longitude", (float) 121.427062);
        else if (xSharedPreferences != null)
            return xSharedPreferences.getFloat("longitude", (float) 121.427062);
        return lng;
    }

    public String getInventory() {
        if (sharedPreferences != null)
            return sharedPreferences.getString("inventory", null);
        else if (xSharedPreferences != null)
            return xSharedPreferences.getString("inventory", null);
        return null;
    }

    public void setInventory(String inventory) {
        SharedPreferences.Editor prefEditor = sharedPreferences.edit();
        prefEditor.putString("inventory", inventory);
        prefEditor.apply();
    }

    public boolean isStarted() {
        if (sharedPreferences != null)
            return sharedPreferences.getBoolean("start", false);
        else if (xSharedPreferences != null)
            return xSharedPreferences.getBoolean("start", false);
        return start;
    }

    public void update(double la, double ln, boolean start) {
        SharedPreferences.Editor prefEditor = sharedPreferences.edit();
        prefEditor.putFloat("latitude",  (float) la);
        prefEditor.putFloat("longitude", (float) ln);
        prefEditor.putBoolean("start",   start);
        prefEditor.apply();
    }

    public void reload() {
        xSharedPreferences.reload();
    }

}
