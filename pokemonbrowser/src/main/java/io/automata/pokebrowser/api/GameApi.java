package io.automata.pokebrowser.api;

import android.content.Context;

import javax.inject.Inject;

/**
 * Created by Andy on 2016/1/8.
 */

public class GameApi {

    public interface RespondListener {

        void sendTrackerScreen(String n);
        void sendTrackerEvent(String a, String c, String l);

    }

    public RespondListener listener;

    static {
        System.loadLibrary("pokemonbrowser");
    }

    @Inject public GameApi(Context context) {
        init(context);
    }

    public native void init(Context context);

    public native String call(String command);

    public void sendTrackerScreen(final String n) {
        if (listener != null) {
            listener.sendTrackerScreen(n);
        }
    }

    public void sendTrackerEvent(final String a, final String c, final String l) {
        if (listener != null) {
            listener.sendTrackerEvent(a, c, l);
        }
    }

}
