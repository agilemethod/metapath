package io.automata.pokebrowser;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.location.Location;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import org.json.JSONObject;

import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import javax.inject.Inject;

import io.automata.pokebrowser.api.GameApi;
import io.automata.pokebrowser.ui.JoystickView;
import io.automata.pokebrowser.ui.MainView;
import io.automata.pokebrowser.ui.NotificationView;
import io.automata.pokebrowser.ui.UiMapView;


public class WindowService extends Service {

    @Inject GameApi mGameApi;

    private WindowManager mWindowManager;
    private MainView mMainMenu;
    private JoystickView mJoystickView;
    private UiMapView mUiMapView;
    private NotificationView mNotificationView;
    private boolean mIsShowNotification = false;
    private boolean mBtnMovable = true;
    private boolean mMapShow = false;
    private long mLocUpdateTime = 0;
    private long mUpdateTime = 500;
    private Timer mTimer;
    private boolean mIsWalking = false;
    private double mDegree;
    private Location mFakeLocation;

    private static final String DEBUG_URL = "http://auto.elggum.com/pokebrowser_dev.html";
    private static final String RELEASE_URL = "http://auto.elggum.com/pokebrowser_dev.html";

    private BroadcastReceiver mRroadcastReceiver = new BroadcastReceiver() {
        @Override public void onReceive(Context context, Intent intent) {
            Log.d("ReceiveBroadcastIntent", intent.getAction());
            if (intent.getAction().equals(LocationHooker.BROADCAST_ACTION_ENCOUNTER)) {
                String json = intent.getStringExtra("encounter");
                //Log.d("Receive Encounter", json);
                try {
                    String msg;
                    JSONObject jobj = new JSONObject(json);
                    JSONObject pokemon = jobj.getJSONObject("pokemon");
                    msg = String.format(Locale.US, "ATK %d | DEF %d | STA %d | CP %d\nChance %.1f%% | %.1f%% | %.1f%%"
                        , pokemon.getInt("individual_attack")
                        , pokemon.getInt("individual_defense")
                        , pokemon.getInt("individual_stamina")
                        , pokemon.getInt("cp")
                        , jobj.getDouble("ITEM_POKE_BALL") * 100
                        , jobj.getDouble("ITEM_GREAT_BALL") * 100
                        , jobj.getDouble("ITEM_ULTRA_BALL") * 100
                    );
                    showNotification(msg);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (intent.getAction().equals(LocationHooker.BROADCAST_ACTION_MAP_POKEMON)) {
                String json = intent.getStringExtra("data");
                Log.d("Receive Map Pokemon", json);
                if (mMainMenu != null) {
                    mMainMenu.newNearPokemon(json);
                }
            }
        }
    };

    public MainView getMainWindowView() {
        ((MainApp) getBaseContext().getApplicationContext()).getAppComponent().inject(this);
        LayoutInflater layoutInflater = LayoutInflater.from(getApplicationContext());
        MainView cmv = (MainView) layoutInflater.inflate(R.layout.autometa_main, null, false);
        if (BuildConfig.DEBUG) {
            cmv.init(MainView.GAME_TYPE_MATASHIFT, mGameApi, DEBUG_URL);
        } else {
            cmv.init(MainView.GAME_TYPE_MATASHIFT, mGameApi, RELEASE_URL);
        }
        return cmv;
    }

    public JoystickView getJoystickView() {
        ((MainApp) getBaseContext().getApplicationContext()).getAppComponent().inject(this);
        LayoutInflater layoutInflater = LayoutInflater.from(getApplicationContext());
        JoystickView cmv = (JoystickView) layoutInflater.inflate(R.layout.joystick, null, false);
        if (BuildConfig.DEBUG) {
            cmv.init();
        } else {
            cmv.init();
        }
        return cmv;
    }

    public UiMapView getMapView() {
        ((MainApp) getBaseContext().getApplicationContext()).getAppComponent().inject(this);
        LayoutInflater layoutInflater = LayoutInflater.from(getApplicationContext());
        UiMapView cmv = (UiMapView) layoutInflater.inflate(R.layout.map_view, null, false);
        if (BuildConfig.DEBUG) {
            cmv.init();
        } else {
            cmv.init();
        }
        return cmv;
    }

    public NotificationView getNotificationView() {
        ((MainApp) getBaseContext().getApplicationContext()).getAppComponent().inject(this);
        LayoutInflater layoutInflater = LayoutInflater.from(getApplicationContext());
        NotificationView cmv = (NotificationView) layoutInflater.inflate(R.layout.notification_view, null, false);
        if (BuildConfig.DEBUG) {
            cmv.init();
        } else {
            cmv.init();
        }
        return cmv;
    }

    @Override public IBinder onBind(Intent intent) {
        return null;
    }

    @Override public void onCreate() {
        super.onCreate();
        Log.d("CommonWindowService", "onCreate!");
        mWindowManager = (WindowManager) getSystemService(WINDOW_SERVICE);

        mMainMenu = getMainWindowView();
        mMainMenu.setOnCloseListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                stopSelf();
            }
        });

        Point size = new Point();
        mWindowManager.getDefaultDisplay().getSize(size);
        final WindowManager.LayoutParams params1 = new WindowManager.LayoutParams(
            (int) getResources().getDimension(R.dimen.g_btn_width),
            (int) getResources().getDimension(R.dimen.g_btn_width),
            WindowManager.LayoutParams.TYPE_PHONE,
            WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH,
            PixelFormat.TRANSLUCENT);

        params1.gravity = Gravity.TOP | Gravity.LEFT;
        params1.x = 0;
        params1.y = 0;
        params1.screenOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;

        final WindowManager.LayoutParams params2 = new WindowManager.LayoutParams(
            WindowManager.LayoutParams.MATCH_PARENT,
            (int) getResources().getDimension(R.dimen.g_height),
            WindowManager.LayoutParams.TYPE_PHONE,
            WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH,
            PixelFormat.TRANSLUCENT);

        params2.gravity = Gravity.TOP | Gravity.LEFT;
        params2.x = 0;
        params2.y = 0;
        params2.screenOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;

        mMainMenu.setOnShowListener(new View.OnClickListener(){
            @Override public void onClick(View v) {
                mWindowManager.updateViewLayout(mMainMenu, params2);
                mBtnMovable = false;
            }
        });

        mMainMenu.setOnHideListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                mWindowManager.updateViewLayout(mMainMenu, params1);
                mBtnMovable = true;
            }
        });

        final Point screenSize = size;
        final int btnWidth = (int) getResources().getDimension(R.dimen.g_btn_width);
        mMainMenu.setMainBtnOnTouchListener(new View.OnTouchListener() {
            @Override public boolean onTouch(View v, MotionEvent event) {
                int touchX = (int) event.getRawX() - btnWidth / 2;
                if (touchX <  btnWidth / 2) touchX = 0;
                if (touchX > screenSize.x - btnWidth) touchX = screenSize.x - btnWidth;
                if (mBtnMovable && Math.abs(touchX - params1.x) > btnWidth / 2) {
                    params1.x = touchX;
                    mWindowManager.updateViewLayout(mMainMenu, params1);
                }
                return false;
            }
        });

        // ============================ //

        mJoystickView = getJoystickView();
        final WindowManager.LayoutParams paramsJoystick1 = new WindowManager.LayoutParams(
                (int) getResources().getDimension(R.dimen.g_joystick_width),
                (int) getResources().getDimension(R.dimen.g_joystick_width),
                WindowManager.LayoutParams.TYPE_SYSTEM_ALERT,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH,
                PixelFormat.TRANSLUCENT);
        paramsJoystick1.gravity = Gravity.TOP | Gravity.LEFT;
        paramsJoystick1.x = (int) getResources().getDimension(R.dimen.g_btn_width);
        paramsJoystick1.y = (int) getResources().getDimension(R.dimen.g_joystick_width);

        final int joystickWidth = (int) getResources().getDimension(R.dimen.g_joystick_width);
        mJoystickView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (mBtnMovable) {
                    if (event.getAction() == MotionEvent.ACTION_MOVE) {
                        int touchX = (int) event.getRawX() - joystickWidth / 2;
                        int touchY = (int) event.getRawY() - joystickWidth / 2;
                        paramsJoystick1.x = touchX;
                        paramsJoystick1.y = touchY;
                        mWindowManager.updateViewLayout(mJoystickView, paramsJoystick1);
                    }
                } else {
                    if (event.getAction() == MotionEvent.ACTION_MOVE) {
                        mJoystickView.sendPosition(event.getX(), event.getY());
                        mDegree = mJoystickView.getDgree();
                    } else if (event.getAction() == MotionEvent.ACTION_DOWN) {
                        startRunning();
                        mJoystickView.sendPosition(event.getX(), event.getY());
                        mDegree = mJoystickView.getDgree();
                    } else if (event.getAction() == MotionEvent.ACTION_UP) {
                        stopRunning();
                        mJoystickView.sendPosition(mJoystickView.getWidth() / 2, mJoystickView.getHeight() / 2);
                    }
                }

                return false;
            }
        });

        // ============================ //
        int height = size.y - (int) getResources().getDimension(R.dimen.g_height2);
        mUiMapView = getMapView();
        final WindowManager.LayoutParams paramsMap1 = new WindowManager.LayoutParams(
            WindowManager.LayoutParams.MATCH_PARENT,
            height,
            WindowManager.LayoutParams.TYPE_SYSTEM_ALERT,
            WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH,
            PixelFormat.TRANSLUCENT);
        paramsMap1.gravity = Gravity.TOP | Gravity.LEFT;
        paramsMap1.x = 0;
        paramsMap1.y = (int) getResources().getDimension(R.dimen.g_height);

        final WindowManager.LayoutParams paramsMap2 = new WindowManager.LayoutParams(
            3,
            3,
            WindowManager.LayoutParams.TYPE_SYSTEM_ALERT,
            WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH,
            PixelFormat.TRANSLUCENT);
        paramsMap2.gravity = Gravity.TOP | Gravity.LEFT;
        paramsMap2.x = 0;
        paramsMap2.y = (int) getResources().getDimension(R.dimen.g_joystick_width);

        mMainMenu.mOnButtonClick = new MainView.OnButtonClick() {
            @Override public void onButtonClick(int type) {
                Log.d("onButtonClick", "onButtonClick" + type);
                boolean isShow = false;
                if (type == MainView.BTN_MAIN) {
                    if (!mMainMenu.isShowing()) {
                        mUiMapView.hide();
                        isShow = false;
                    }
                } else if (type == MainView.BTN_START) {
                    isShow = mUiMapView.toggleMap();
                } else if (type == MainView.BTN_STOP) {
                    isShow = mUiMapView.togglePoke();
                    if (isShow) {
                        String pokemonListText = mMainMenu.mSettings.getInventory();
                        if (pokemonListText != null) {
                            mUiMapView.updatePokemonList(pokemonListText);
                        }
                    }
                } else if (type == MainView.BTN_CLOSE) {
                    showNotification("Your fake GPS will be shutdown.\nSo make sure you close App after go home first.\nOne more click to close.");
                }
                if (isShow) {
                    if (!mMapShow) {
                        mWindowManager.addView(mUiMapView, paramsMap1);
                        mMapShow = true;
                        if (type == MainView.BTN_START) {
                            mWindowManager.removeView(mJoystickView);
                            mWindowManager.addView(mJoystickView, paramsJoystick1);
                        }
                    }
                } else {
                    if (mMapShow) {
                        mWindowManager.removeView(mUiMapView);
                        mMapShow = false;
                    }
                }
            }
        };

        mMainMenu.mUiMapView = mUiMapView;
        mMainMenu.mapInit();

        // ============================ //
        mNotificationView = getNotificationView();

        //mWindowManager.addView(mUiMapView, paramsMap2);
        mWindowManager.addView(mJoystickView, paramsJoystick1);
        mWindowManager.addView(mMainMenu, params1);
        mMainMenu.toggleDisplayView();

        IntentFilter filter = new IntentFilter(LocationHooker.BROADCAST_ACTION_INVENTORY);
        filter.addAction(LocationHooker.BROADCAST_ACTION_ENCOUNTER);
        filter.addAction(LocationHooker.BROADCAST_ACTION_CATCHED);
        filter.addAction(LocationHooker.BROADCAST_ACTION_MAP_POKEMON);
        registerReceiver(mRroadcastReceiver, filter);

        Log.d("getInventory", getInventory());
    }

    @Override public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mRroadcastReceiver);
        showNotification("See you next time");
        if (mMainMenu != null) {
            mWindowManager.removeView(mMainMenu);
            mWindowManager.removeView(mJoystickView);
            if (mUiMapView.mMapView != null) {
                mUiMapView.mMapView.onPause();
                mUiMapView.mMapView.onDestroy();
            }
            if (mMapShow) {
                mWindowManager.removeView(mUiMapView);
            }
        }
    }

    public void showNotification(final String text) {
        final WindowManager.LayoutParams paramsNotification = new WindowManager.LayoutParams(
            WindowManager.LayoutParams.MATCH_PARENT,
            (int) getResources().getDimension(R.dimen.g_btn_width),
            WindowManager.LayoutParams.TYPE_SYSTEM_ALERT,
            WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH,
            PixelFormat.TRANSLUCENT);
        paramsNotification.gravity = Gravity.TOP | Gravity.LEFT;
        paramsNotification.x = 0;
        paramsNotification.y = (int) getResources().getDimension(R.dimen.g_btn_width);
        ((TextView)mNotificationView.findViewById(R.id.msg)).setText(text);
        if (!mIsShowNotification) {
            mWindowManager.addView(mNotificationView, paramsNotification);
            mIsShowNotification = true;
        }
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override public void run() {
                if (mIsShowNotification) {
                    mWindowManager.removeView(mNotificationView);
                    mIsShowNotification = false;
                }
            }
        }, 5500);
    }

    private double degree(int x, int y, int r) {
        x -= r;
        y -= r;
        y = -y;
        return -Math.toDegrees(Math.atan2(y, x)) + 80;
    }

    public void startRunning() {
        if(mTimer != null) {
            return;
        }
        Log.d("CallApp", "Start Running");
        mTimer = new Timer();
        mTimer.scheduleAtFixedRate(new TimerTask() {
            @Override public void run() {
                if (!mBtnMovable && System.currentTimeMillis() - mLocUpdateTime > mUpdateTime) {
                    float dist = mMainMenu.basicSpeed * mMainMenu.speedRatio;
                    Location latLng = destVincenty(mMainMenu.mFakeLocation.getLatitude()
                        , mMainMenu.mFakeLocation.getLongitude(), mDegree, dist);
                    mMainMenu.mFakeLocation.setLatitude(latLng.getLatitude());
                    mMainMenu.mFakeLocation.setLongitude(latLng.getLongitude());
                    mMainMenu.mSettings.update(latLng.getLatitude(), latLng.getLongitude(), true);
                    Log.d("CallApp", "D " + mDegree + " Update location " + latLng.toString());
                    mLocUpdateTime = System.currentTimeMillis();
                    mMainMenu.updateText();
                    mMainMenu.updateFakeMarker();
                }
            }
        }, 500, 500);
    }

    public void stopRunning() {
        if (mTimer == null) {
            return;
        }
        Log.d("CallApp", "Stop Running");
        mTimer.cancel();
        mTimer = null;
    }

    private static Location destVincenty(final double latitude, final double longitude, final double angle,
        final double distanceInMeters) {
        // WGS-84 ellipsiod
        final double semiMajorAxis = 6378137;
        final double b = 6356752.3142;
        final double inverseFlattening = 1 / 298.257223563;
        final double alpha1 = Math.toRadians(angle);
        final double sinAlpha1 = Math.sin(alpha1);
        final double cosAlpha1 = Math.cos(alpha1);

        final double tanU1 = (1 - inverseFlattening) * Math.tan(Math.toRadians(latitude));
        final double cosU1 = 1 / Math.sqrt(1 + tanU1 * tanU1), sinU1 = tanU1 * cosU1;
        final double sigma1 = Math.atan2(tanU1, cosAlpha1);
        final double sinAlpha = cosU1 * sinAlpha1;
        final double cosSqAlpha = 1 - sinAlpha * sinAlpha;
        final double uSq = cosSqAlpha * (semiMajorAxis * semiMajorAxis - b * b) / (b * b);
        final double aa = 1 + uSq / 16384 * (4096 + uSq * (-768 + uSq * (320 - 175 * uSq)));
        final double ab = uSq / 1024 * (256 + uSq * (-128 + uSq * (74 - 47 * uSq)));

        double sigma = distanceInMeters / (b * aa);
        double sigmaP = 2 * Math.PI;
        double sinSigma = 0;
        double cosSigma = 0;
        double cos2SigmaM = 0;
        double deltaSigma = 0;
        while (Math.abs(sigma - sigmaP) > 1e-12) {
            cos2SigmaM = Math.cos(2 * sigma1 + sigma);
            sinSigma = Math.sin(sigma);
            cosSigma = Math.cos(sigma);
            deltaSigma = ab
                * sinSigma
                * (cos2SigmaM + ab
                / 4
                * (cosSigma * (-1 + 2 * cos2SigmaM * cos2SigmaM) - ab / 6 * cos2SigmaM
                * (-3 + 4 * sinSigma * sinSigma) * (-3 + 4 * cos2SigmaM * cos2SigmaM)));
            sigmaP = sigma;
            sigma = distanceInMeters / (b * aa) + deltaSigma;
        }

        final double tmp = sinU1 * sinSigma - cosU1 * cosSigma * cosAlpha1;
        final double lat2 = Math.atan2(sinU1 * cosSigma + cosU1 * sinSigma * cosAlpha1,
            (1 - inverseFlattening) * Math.sqrt(sinAlpha * sinAlpha + tmp * tmp));
        final double lambda = Math.atan2(sinSigma * sinAlpha1, cosU1 * cosSigma - sinU1 * sinSigma * cosAlpha1);
        final double c = inverseFlattening / 16 * cosSqAlpha * (4 + inverseFlattening * (4 - 3 * cosSqAlpha));
        final double l = lambda - (1 - c) * inverseFlattening * sinAlpha
            * (sigma + c * sinSigma * (cos2SigmaM + c * cosSigma * (-1 + 2 * cos2SigmaM * cos2SigmaM)));

        Location latLng = new Location("");
        latLng.setLatitude(Math.toDegrees(lat2));
        latLng.setLongitude(longitude + Math.toDegrees(l));
        return latLng;
    }

    public String getInventory() {
        return "{\"pokemons\":[]}";
    }

}
