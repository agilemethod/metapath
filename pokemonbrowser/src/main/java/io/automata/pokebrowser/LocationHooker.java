package io.automata.pokebrowser;

import android.app.AndroidAppHelper;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;

import com.github.aeonlucid.pogoprotos.Data;
import com.github.aeonlucid.pogoprotos.Inventory;
import com.github.aeonlucid.pogoprotos.data.Capture;
import com.github.aeonlucid.pogoprotos.map.Pokemon;
import com.github.aeonlucid.pogoprotos.networking.Envelopes;
import com.github.aeonlucid.pogoprotos.networking.Requests;
import com.github.aeonlucid.pogoprotos.networking.Responses;
import com.google.protobuf.ByteString;
import com.google.protobuf.Descriptors;
import com.google.protobuf.InvalidProtocolBufferException;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.net.HttpURLConnection;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import de.robv.android.xposed.IXposedHookLoadPackage;
import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.XposedBridge;
import de.robv.android.xposed.XposedHelpers;
import de.robv.android.xposed.callbacks.XC_LoadPackage;

/**
 * Created by Andy on 2016/8/28.
 */
public class LocationHooker implements IXposedHookLoadPackage {

    public static final String BROADCAST_ACTION_INVENTORY = "io.automata.pokebrowser.inventory";
    public static final String BROADCAST_ACTION_ENCOUNTER = "io.automata.pokebrowser.encounter";
    public static final String BROADCAST_ACTION_CATCHED = "io.automata.pokebrowser.catched";
    public static final String BROADCAST_ACTION_MAP_POKEMON = "io.automata.pokebrowser.map.pokemon";

    public static final String PACKAGE_NAME = "io.automata.pokegorunner";

    private static Context mContext = null;
    private static Context mCurrentContext = null;

    private Class mNiaNetClass;
    private static final Map<Long, List<Requests.RequestType>> requestMap = new HashMap<>();
    private static Settings mSettings = new Settings();
    private boolean mIsStart = false;
    private double mNewLat = 25.021172;
    private double mNewLng = 121.427062;
    private long mLastReloadTime = 0;
    private LocationListener mLocationListener;
    private Method mOnLocationChanged;
    private Timer mTimer;
    private String mInventory = "{\"pokemons\":[]}";

    public String mTargetPackage = "com.nianticlabs.pokemongo";

    private void updateSetting() {
        if (System.currentTimeMillis() - mLastReloadTime > 500) {
            mSettings.reload();
            mIsStart = mSettings.isStarted();
            mNewLat = mSettings.getLat();
            mNewLng = mSettings.getLng();
        }
    }

    // Hook Location method
    XC_MethodHook locationMethodHooker = new XC_MethodHook() {
        @Override protected void afterHookedMethod(XC_MethodHook.MethodHookParam param) throws Throwable {
            updateSetting();
            if (param.method.getName().equals("getTime")) {
                if (mIsStart) {
                    param.setResult(System.currentTimeMillis());
                }
            }
            if (param.method.getName().equals("getLatitude")) {
                if (mIsStart) {
                    param.setResult(mNewLat);
                }
            }
            if (param.method.getName().equals("getLongitude")) {
                if (mIsStart) {
                    param.setResult(mNewLng);
                }
            }
            if (param.method.getName().equals("getProvider")) {
                //param.setResult(new String("fused"));
            }
            if (param.method.getName().equals("getAccuracy")) {
                //double acc = (double) (rand.nextInt(20) - 10);
                //param.setResult(new Double(acc));
            }
        }
    };

    // Hook Location method
    XC_MethodHook managerMethodHooker = new XC_MethodHook() {
        @Override protected void afterHookedMethod(XC_MethodHook.MethodHookParam param) throws Throwable {
            if (param.method.getName().equals("getScanResults")) {
                if (mIsStart) {
                    param.setResult(null);
                }
            }
            if (param.method.getName().equals("getCellLocation")) {
                if (mIsStart) {
                    param.setResult(null);
                }
            }
            if (param.method.getName().equals("getNeighboringCellInfo")) {
                if (mIsStart) {
                    param.setResult(null);
                }
            }
        }
    };

    XC_MethodHook requestLocationUpdateHooker = new XC_MethodHook() {
        @Override protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
            for (int count = 0; count < param.args.length; count++) {
                if (param.args[count] instanceof LocationListener) {
                    param.args[1] = 1000;
                    LocationListener ll = (LocationListener) param.args[count];
                    Class<?> clazz = LocationListener.class;
                    Method m = null;
                    for (Method method : clazz.getDeclaredMethods()) {
                        if (method.getName().equals("onLocationChanged")) {
                            m = method;
                            break;
                        }
                    }
                    mLocationListener = ll;
                    mOnLocationChanged = m;
                    try {
                        updateSetting();
                        if (m != null && mIsStart) {
                            Object[] args = new Object[1];
                            Location l = new Location(LocationManager.GPS_PROVIDER);
                            double la = mNewLat;
                            double lo = mNewLng;
                            l.setLatitude(la);
                            l.setLongitude(lo);
                            l.setAltitude(63.7);
                            l.setAccuracy(9.56f);
                            l.setTime(System.currentTimeMillis());
                            args[0] = l;
                            m.invoke(ll, args);
                        }
                    } catch (Exception e) {
                        Log(e.toString());
                    }
                }
            }
        }
    };

    XC_MethodHook getGpsStatusHooker = new XC_MethodHook() {
        @Override protected void afterHookedMethod(MethodHookParam param) throws Throwable {
            GpsStatus gss = (GpsStatus) param.getResult();
            if (gss == null)
                return;
            Class<?> clazz = GpsStatus.class;
            Method m = null;
            for (Method method : clazz.getDeclaredMethods()) {
                if (method.getName().equals("setStatus")) {
                    if (method.getParameterTypes().length > 1) {
                        m = method;
                        break;
                    }
                }
            }
            if (m == null) {
                return;
            }
            //access the private setStatus function of GpsStatus
            m.setAccessible(true);

            //make the apps believe GPS works fine now
            int svCount = 5;
            int[] prns = {1, 2, 3, 4, 5};
            float[] snrs = {0, 0, 0, 0, 0};
            float[] elevations = {0, 0, 0, 0, 0};
            float[] azimuths = {0, 0, 0, 0, 0};
            int ephemerisMask = 0x1f;
            int almanacMask = 0x1f;

            //5 satellites are fixed
            int usedInFixMask = 0x1f;

            try {
                m.invoke(gss,svCount, prns, snrs, elevations, azimuths, ephemerisMask, almanacMask, usedInFixMask);
                param.setResult(gss);
            } catch (Exception e) {
                Log(e.toString());
            }
        }
    };

    XC_MethodHook doSyncRequestHooker = new XC_MethodHook() {
        @Override protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
            ByteBuffer requestBody = (ByteBuffer) param.args[5];
            int bodyOffset = (int) param.args[6];
            int bodySize = (int) param.args[7];

            if (requestBody == null || bodySize <= 0) {
                Log("Error while getting data, requestBody =" + bodySize + " or bodySize = " + bodySize + " is not valid");
                return;
            }

            ByteBuffer dRequestBody = requestBody.duplicate();
            byte[] bytes = new byte[bodySize];
            dRequestBody.get(bytes, bodyOffset, bodySize);

            Envelopes.RequestEnvelope requestEnvelop = Envelopes.RequestEnvelope.parseFrom(bytes);
            long requestId = requestEnvelop.getRequestId();

            List<Requests.RequestType> requestList = new ArrayList<Requests.RequestType>();
            for (int i = 0; i < requestEnvelop.getRequestsCount(); i++) {
                Log("doSyncRequest - " + requestEnvelop.getRequests(i).getRequestType().toString());
                requestList.add(requestEnvelop.getRequests(i).getRequestType());
            }

            requestMap.put(requestId, requestList);
        }
    };

    XC_MethodHook readDataSteam = new XC_MethodHook() {
        @Override protected void afterHookedMethod(MethodHookParam param) throws Throwable {
            int bodySize = (int) param.getResult();
            byte[] bytes = new byte[bodySize];

            Field bufferField = mNiaNetClass.getDeclaredField("readBuffer");
            bufferField.setAccessible(true);

            ThreadLocal<ByteBuffer> localBody = (ThreadLocal<ByteBuffer>) bufferField.get(null);

            if (localBody == null) {
                Log("Couldn't read readBuffer-stream from PoGo.");
                return;
            }

            ByteBuffer responseBody = localBody.get();
            ByteBuffer dResponseBody = responseBody.duplicate();
            dResponseBody.get(bytes, 0, bodySize);
            HandleResponse(bytes);
        }
    };

    public void handleLoadPackage(final XC_LoadPackage.LoadPackageParam lpparam) throws Throwable {
        if (!lpparam.packageName.equals(mTargetPackage)) {
            return;
        }
        updateSetting();
        hook_method("android.location.Location", lpparam.classLoader, "getTime", locationMethodHooker);
        hook_method("android.location.Location", lpparam.classLoader, "getLatitude", locationMethodHooker);
        hook_method("android.location.Location", lpparam.classLoader, "getLongitude", locationMethodHooker);
        hook_method("android.location.Location", lpparam.classLoader, "getProvider", locationMethodHooker);
        hook_method("android.location.Location", lpparam.classLoader, "getAccuracy", locationMethodHooker);

        hook_method("android.net.wifi.WifiManager", lpparam.classLoader, "getScanResults", managerMethodHooker);
        hook_method("android.telephony.TelephonyManager", lpparam.classLoader, "getCellLocation", managerMethodHooker);
        hook_method("android.telephony.TelephonyManager", lpparam.classLoader, "getNeighboringCellInfo", managerMethodHooker);

        hook_method("android.location.LocationManager", "requestLocationUpdates", requestLocationUpdateHooker);
        hook_method("android.location.LocationManager", "getGpsStatus", getGpsStatusHooker);

        mNiaNetClass = lpparam.classLoader.loadClass("com.nianticlabs.nia.network.NiaNet");
        hook_method(mNiaNetClass, "doSyncRequest", long.class, int.class, String.class, int.class, String.class, ByteBuffer.class, int.class, int.class, doSyncRequestHooker);
        hook_method(mNiaNetClass, "readDataSteam", HttpURLConnection.class, readDataSteam);

        //startRunning();
    }

    public void startRunning() {
        if(mTimer != null) {
            return;
        }
        mTimer = new Timer();
        mTimer.scheduleAtFixedRate(new TimerTask() {
            @Override public void run() {
                updateSetting();
                if (mIsStart && mLocationListener != null && mOnLocationChanged != null) {
                    Object[] args = new Object[1];
                    Location l = new Location(LocationManager.GPS_PROVIDER);
                    l.setTime(System.currentTimeMillis());
                    l.setLatitude(mNewLat);
                    l.setLongitude(mNewLng);
                    l.setAltitude(63.7);
                    l.setAccuracy(9.56f);
                    args[0] = l;
                    try {
                        mOnLocationChanged.invoke(mLocationListener, args);
                    } catch (Exception e) {
                        Log(e.toString());
                    }
                    //XposedHelpers.callMethod(mLocationListener, "onLocationChanged", l);
                }
            }
        }, 500, 1000);
    }

    public void stopRunning() {
        if (mTimer == null) {
            return;
        }
        mTimer.cancel();
        mTimer = null;
    }

    private void HandleResponse(byte[] buffer) {
        Envelopes.ResponseEnvelope responseEnvelop;
        try {
            responseEnvelop = Envelopes.ResponseEnvelope.parseFrom(buffer);
        } catch (InvalidProtocolBufferException e) {
            Log("Parsing response failed " + e);
            return;
        }

        long requestId = responseEnvelop.getRequestId();
        if (requestId == 0 || !requestMap.containsKey(requestId)) {
            Log("requestId is 0 or not in requestMap | requestId = " + requestId);
            return;
        }

        Log("Response " + requestId);
        List<Requests.RequestType> requestList = requestMap.get(requestId);

        for (int i = 0; i < requestList.size(); i++) {
            Requests.RequestType requestType = requestList.get(i);
            ByteString payload = responseEnvelop.getReturns(i);
            Log("HandleResponse " + requestType.toString());

            switch (requestType) {
            case ENCOUNTER:
                Encounter(payload); // wild encounter
                break;
            case DISK_ENCOUNTER:
                DiskEncounter(payload); // lured encounter
                break;
            case INCENSE_ENCOUNTER:
                IncenseEncounter(payload); // incense encounter
                break;
            case CATCH_POKEMON:
                Catch(payload);
                break;
            case GET_INVENTORY:
                GetInventory(payload);
                break;
            case GET_MAP_OBJECTS:
                GetMapObjects(payload);
                break;
            }
        }
    }

    private void GetMapObjects(ByteString payload) {
        Responses.GetMapObjectsResponse getMapObjectsResponse;
        try {
            getMapObjectsResponse = Responses.GetMapObjectsResponse.parseFrom(payload);
        } catch (InvalidProtocolBufferException e) {
            Log("Parsing GetMapObjectsResponse failed " + e);
            return;
        }

        for (com.github.aeonlucid.pogoprotos.Map.MapCell mapCell : getMapObjectsResponse.getMapCellsList()) {
            for (Pokemon.MapPokemon mapPokemon : mapCell.getCatchablePokemonsList()) {
                try {
                    JSONObject mp = new JSONObject();
                    mp.put("encounter_id", mapPokemon.getEncounterId());
                    mp.put("pokemon_id", mapPokemon.getPokemonIdValue());
                    mp.put("latitude", mapPokemon.getLatitude());
                    mp.put("longitude", mapPokemon.getLongitude());
                    mp.put("hidden_time", mapPokemon.getExpirationTimestampMs());
                    Intent intent = new Intent(BROADCAST_ACTION_MAP_POKEMON);
                    intent.putExtra("data", mp.toString());
                    sendBroadcast(intent);
                } catch (Exception e) {}
                //Log("CatchablePokemon = ", mapPokemon.getAllFields().entrySet());
            }
            for (Pokemon.WildPokemon wildPokemon : mapCell.getWildPokemonsList()) {
                try {
                    JSONObject mp = new JSONObject();
                    mp.put("encounter_id", wildPokemon.getEncounterId());
                    mp.put("pokemon_id", wildPokemon.getPokemonData().getPokemonIdValue());
                    mp.put("latitude", wildPokemon.getLatitude());
                    mp.put("longitude", wildPokemon.getLongitude());
                    mp.put("hidden_time", wildPokemon.getLastModifiedTimestampMs() + wildPokemon.getTimeTillHiddenMs());
                    Intent intent = new Intent(BROADCAST_ACTION_MAP_POKEMON);
                    intent.putExtra("data", mp.toString());
                    sendBroadcast(intent);
                } catch (Exception e) {}
                //Log("WildPokemon = ", wildPokemon.getAllFields().entrySet());
            }
//            for (Pokemon.NearbyPokemon nearbyPokemon : mapCell.getNearbyPokemonsList()) {
//                //Log("NearbyPokemon = ", nearbyPokemon.getAllFields().entrySet());
//            }
        }
        //Log("getMapObjects = ", getMapObjectsResponse.getAllFields().entrySet());
    }

    private void GetInventory(ByteString payload) {
        Responses.GetInventoryResponse getInventoryResponse;
        try {
            getInventoryResponse = Responses.GetInventoryResponse.parseFrom(payload);
        } catch (InvalidProtocolBufferException e) {
            Log("Parsing GetInventoryResponse failed " + e);
            return;
        }
        if (getInventoryResponse.getInventoryDelta().getOriginalTimestampMs() == 0) {
            String json = inventoryToJson(getInventoryResponse.getInventoryDelta());
            Intent intent = new Intent(BROADCAST_ACTION_INVENTORY);
            intent.putExtra("data", json);
            sendBroadcast(intent);
            Log("GetInventory JSON string " + json);
        }
        Log("getInventory = ", getInventoryResponse.getAllFields().entrySet());
    }

    private String inventoryToJson(Inventory.InventoryDelta delta) {
        JSONObject root = new JSONObject();
        JSONArray pokemons = new JSONArray();
        for (Inventory.InventoryItem item : delta.getInventoryItemsList()) {
            if (item.hasInventoryItemData() && item.getInventoryItemData().hasPokemonData()) {
                Data.PokemonData data = item.getInventoryItemData().getPokemonData();
                if (data.getIsEgg()) {
                    continue;
                }
                JSONObject pokemon = pokemonDataToJson(data);
                if (pokemon != null) {
                    pokemons.put(pokemon);
                }
            }
        }
        try {
            root.put("pokemons", pokemons);
        } catch (Exception e) {}
        return root.toString();
    }

    private JSONObject pokemonDataToJson(Data.PokemonData data) {
        JSONObject pokemon = new JSONObject();
        try {
            pokemon.put("id", data.getId());
            pokemon.put("pokemon_id", data.getPokemonIdValue());
            pokemon.put("cp", data.getCp());
            pokemon.put("stamina_max", data.getStaminaMax());
            pokemon.put("individual_attack", data.getIndividualAttack());
            pokemon.put("individual_defense", data.getIndividualDefense());
            pokemon.put("individual_stamina", data.getIndividualStamina());
            pokemon.put("cp_multiplier", data.getCpMultiplier());
            pokemon.put("creation_time_ms", data.getCreationTimeMs());
            pokemon.put("nickname", data.getNickname());
            pokemon.put("move_1", data.getMove1Value());
            pokemon.put("move_2", data.getMove2Value());
        } catch (Exception e) {
            return null;
        }
        return pokemon;
    }

    private void Encounter(ByteString payload) {
        Responses.EncounterResponse encounterResponse;
        try {
            encounterResponse = Responses.EncounterResponse.parseFrom(payload);
        } catch (InvalidProtocolBufferException e) {
            Log("Parsing EncounterResponse failed " + e);
            return;
        }
        com.github.aeonlucid.pogoprotos.Data.PokemonData encounteredPokemon = encounterResponse.getWildPokemon().getPokemonData();
        Capture.CaptureProbability captureProbability = encounterResponse.getCaptureProbability();
        encounter(encounteredPokemon, captureProbability);

        Log("encounterResponse = ", encounterResponse.getAllFields().entrySet());
        //createEncounterNotification(encounteredPokemon, captureProbability);

    }

    private void IncenseEncounter(ByteString payload) {
        Responses.IncenseEncounterResponse incenseEncounterResponse;
        try {
            incenseEncounterResponse = Responses.IncenseEncounterResponse.parseFrom(payload);
        } catch (InvalidProtocolBufferException e) {
            Log("Parsing IncenseEncounterResponse failed " + e);
            return;
        }

        com.github.aeonlucid.pogoprotos.Data.PokemonData encounteredPokemon = incenseEncounterResponse.getPokemonData();
        Capture.CaptureProbability captureProbability = incenseEncounterResponse.getCaptureProbability();
        encounter(encounteredPokemon, captureProbability);

        Log("IncenseEncounter = ", incenseEncounterResponse.getAllFields().entrySet());
        //createEncounterNotification(encounteredPokemon, captureProbability);
    }

    private void DiskEncounter(ByteString payload) {
        Responses.DiskEncounterResponse diskEncounterResponse;
        try {
            diskEncounterResponse = Responses.DiskEncounterResponse.parseFrom(payload);
        } catch (InvalidProtocolBufferException e) {
            Log("Parsing DiskEncounterResponse failed " + e);
            return;
        }

        com.github.aeonlucid.pogoprotos.Data.PokemonData encounteredPokemon = diskEncounterResponse.getPokemonData();
        Capture.CaptureProbability captureProbability = diskEncounterResponse.getCaptureProbability();
        encounter(encounteredPokemon, captureProbability);

        Log("DiskEncounterResponse = ", diskEncounterResponse.getAllFields().entrySet());
        //createEncounterNotification(encounteredPokemon, captureProbability);
    }

    private void encounter(Data.PokemonData data, Capture.CaptureProbability probability) {
        JSONObject root = new JSONObject();
        JSONObject pokemon = pokemonDataToJson(data);
        if (pokemon != null) {
            try {
                root.put("pokemon", pokemon);
                root.put("ITEM_POKE_BALL", probability.getCaptureProbability(0));
                root.put("ITEM_GREAT_BALL", probability.getCaptureProbability(1));
                root.put("ITEM_ULTRA_BALL", probability.getCaptureProbability(2));

                Log("Encounter JSON string " + root.toString());
                Intent intent = new Intent(BROADCAST_ACTION_ENCOUNTER);
                intent.putExtra("encounter", root.toString());
                sendBroadcast(intent);
            } catch (Exception e) {
                return;
            }
        }
    }

    private void sendBroadcast(Intent intent) {
        getCurrentContext().sendBroadcast(intent);
    }

    private void Catch(ByteString payload) {
        Responses.CatchPokemonResponse catchPokemonResponse;
        try {
            catchPokemonResponse = Responses.CatchPokemonResponse.parseFrom(payload);
        } catch (InvalidProtocolBufferException e) {
            Log("Parsing CatchPokemonResponse failed " + e);
            return;
        }
        Intent intent = new Intent(BROADCAST_ACTION_CATCHED);
        intent.putExtra("catchResult", catchPokemonResponse.getStatusValue());
        sendBroadcast(intent);

        Log("catchPokemonResponse = ", catchPokemonResponse.getAllFields().entrySet());
        //Helper.showToast(Helper.getCatchName(catchPokemonResponse.getStatus()), Toast.LENGTH_SHORT);
    }

    public static void Log(String message) {
        if (BuildConfig.DEBUG) {
            XposedBridge.log(message);
        }
    }
    
    public static void Log(String message, Set<Map.Entry<Descriptors.FieldDescriptor, Object>> entries) {
        for (Map.Entry<Descriptors.FieldDescriptor, Object> entry : entries) {
            Log(message + entry.getKey() + " - " + entry.getValue());
        }

    }

    private void hook_method(Class<?> clazz, String methodName, Object... parameterTypesAndCallback) {
        try {
            XposedHelpers.findAndHookMethod(clazz, methodName, parameterTypesAndCallback);
        } catch (Exception e) {
            Log(e.toString());
        }
    }

    private void hook_method(String className, ClassLoader classLoader, String methodName, Object... parameterTypesAndCallback) {
        try {
            XposedHelpers.findAndHookMethod(className, classLoader, methodName, parameterTypesAndCallback);
        } catch (Exception e) {
            Log(e.toString());
        }
    }
    private void hook_method(String className, String methodName, XC_MethodHook xmh) {
        try {
            Class<?> clazz = Class.forName(className);
            for (Method method : clazz.getDeclaredMethods())
                if (method.getName().equals(methodName)
                    && !Modifier.isAbstract(method.getModifiers())
                    && Modifier.isPublic(method.getModifiers())) {
                    XposedBridge.hookMethod(method, xmh);
                }
        } catch (Exception e) {
            Log(e.toString());
        }
    }

    public static Context getContext() {
        if (mContext == null) {
            try {
                mContext = AndroidAppHelper.currentApplication().createPackageContext(PACKAGE_NAME, Context.CONTEXT_IGNORE_SECURITY);
            } catch (PackageManager.NameNotFoundException e) {}
        }
        return mContext;
    }

    public static Context getCurrentContext() {
        if (mCurrentContext == null) {
            mCurrentContext = AndroidAppHelper.currentApplication();
        }
        return mCurrentContext;
    }

}
