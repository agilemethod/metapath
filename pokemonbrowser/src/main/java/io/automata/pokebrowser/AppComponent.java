package io.automata.pokebrowser;

import javax.inject.Singleton;

import dagger.Component;
import io.automata.pokebrowser.api.GameApi;

/**
 * Created by Andy on 2016/1/14.
 */
@Singleton
@Component(modules={AppModule.class})
public interface AppComponent {

    void inject(MainActivity activity);
    void inject(WindowService view);

    GameApi providesGameApi();

}
