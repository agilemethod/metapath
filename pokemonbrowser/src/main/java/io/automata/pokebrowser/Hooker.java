package io.automata.pokebrowser;

import android.app.AndroidAppHelper;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import de.robv.android.xposed.IXposedHookLoadPackage;
import de.robv.android.xposed.IXposedHookZygoteInit;
import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.XposedBridge;
import de.robv.android.xposed.XposedHelpers;
import de.robv.android.xposed.callbacks.XC_LoadPackage;

import static de.robv.android.xposed.XposedHelpers.findClass;

/**
 * Created by Andy on 2016/8/22.
 */

public class Hooker implements IXposedHookLoadPackage, IXposedHookZygoteInit {

    private boolean mLocationManagerHooked = false;
    private Hooker mInstance = this;
    private String mApp = "";
    private HashMap<Method, XC_MethodHook> mHook = new HashMap<Method, XC_MethodHook>();
    private double pi  = 3.14159265359;
    private double newlat = 25.021172;
    private double newlng = 121.427062;
    private Random rand = new Random();
    private final double earth = (double) 6378137.0;
    private static Settings settings = new Settings();
    //private static String HOOK_TARGET = "com.google.android.apps.maps";
    private static String HOOK_TARGET = "com.nianticlabs.pokemongo";
    private LocationListener mLocationListener;

    private void hookLocationMethods(XC_MethodHook.MethodHookParam param) {
        if (param.method.getName().equals("getTime")) {
            // XposedBridge.log("Location Manager getTime " + param.getResult());
            param.setResult(new Long(System.currentTimeMillis()));
        }
        if (param.method.getName().equals("getLatitude")) {
            // XposedBridge.log("Location Manager getLatitude " + newlat + " " + newlng);
            param.setResult(new Double(newlat));
        }
        if (param.method.getName().equals("getLongitude")) {
            param.setResult(new Double(newlng));
        }
        if (param.method.getName().equals("getProvider")) {
            //param.setResult(new String("fused"));
        }
        if (param.method.getName().equals("getAccuracy")) {
            //double acc = (double) (rand.nextInt(20) - 10);
            //param.setResult(new Double(acc));
        }
        if (param.args != null && param.args.length == 1 && param.method.getName().equals("set")) {
            // XposedBridge.log("Location Manager set " + param.args[0]);
        }
    }

    private void hookLocationManager(XC_MethodHook.MethodHookParam param) {
        if (param.args != null && param.args.length == 1 && param.method.getName().equals("isProviderEnabled")) {
            // XposedBridge.log("Location Manager isProviderEnabled : " + param.args[0] + " " + param.getResult());
            if (settings.isStarted()) param.setResult(new Boolean(true));
        }
        if (param.args != null && param.args.length >= 1 && param.method.getName().equals("requestLocationUpdates")) {
            for (int count = 0; count < param.args.length; count++) {
                XposedBridge.log("Location Manager requestLocationUpdates : " + param.args[count]);
                if (param.args[count] instanceof LocationListener) {
                    LocationListener ll = (LocationListener) param.args[count];
                    if (param.args[0] instanceof String && param.args[0] == "gps") {
                        XposedBridge.log("Location get GPS listener");
                        mLocationListener = ll;
                    }
                    try {
                        Class<?> clazz = ll.getClass();
                        for (Method method : clazz.getDeclaredMethods()) {
                            int m = method.getModifiers();
                            if (method != null && Modifier.isPublic(m) && !Modifier.isStatic(m)) {
                                if (!mHook.containsKey(method)) {
                                    mHook.put(method, mMethodHook);
                                    // XposedBridge.log("Location Manager Listener " + method.getName());
                                    XposedBridge.hookMethod(method, mMethodHook);
                                    if (settings.isStarted()) {
                                        //startRunning();
                                        Location l = new Location("network");
                                        updateLocation();
                                        //XposedBridge.log("Location Manager New Loc : " + newlat + " " + newlng);
                                        l.setTime(new Long(System.currentTimeMillis()));
                                        l.setLatitude(newlat);
                                        l.setLongitude(newlng);
                                        l.setAccuracy((float) 23.3);
                                        XposedHelpers.callMethod(ll, "onLocationChanged", l);
                                    }
                                }
                            }
                        }
                    } catch (Exception ex) {
                    }
                }
            }
        }
        if (param.args != null && param.args.length == 1 && param.method.getName().equals("onLocationChanged")) {
            if (param.args[0] instanceof Location) {
                Location ll = (Location) param.args[0];
                try {
                    Class<?> clazz = ll.getClass();
                    for (Method method : clazz.getDeclaredMethods()) {
                        int m = method.getModifiers();
                        if (method != null && Modifier.isPublic(m) && !Modifier.isStatic(m)) {
                            if (!mHook.containsKey(method)) {
                                mHook.put(method, mMethodHook);
                                // XposedBridge.log("Location Manager Listener " + method.getName());
                                XposedBridge.hookMethod(method, mMethodHook);
                            }
                        }
                    }
                } catch (Exception ex) {
                }
            }
        }
        if (param.method.getName().equals("removeUpdates")) {
            //stopRunning();
            mLocationListener = null;
            Set<Method> mMethod = mHook.keySet();
            for (Method m : mMethod) {
                XposedBridge.unhookMethod(m, mHook.get(m));
            }
            mHook.clear();
        }
    }

    XC_MethodHook mMethodHook = new XC_MethodHook() {
        @Override
        protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
            settings.reload();
            if (param.method.getName().equals("onProviderDisabled")) {
                if (settings.isStarted()) param.setResult(null);
            }
        }

        @Override
        protected void afterHookedMethod(MethodHookParam param) throws Throwable {
            settings.reload();
            updateLocation();
            if (!param.hasThrowable()) {
                try {
                    if (settings.isStarted()) {
                        hookLocationMethods(param);
                    }
                    if (param.args != null && param.args.length > 0 && param.args[0] != null) {
                        hookLocationManager(param);
                    }
                } catch (Throwable ex) {
                    throw ex;
                }
            }
        }
    };

    @Override
    public void handleLoadPackage(XC_LoadPackage.LoadPackageParam lpparam) throws Throwable {
        settings.reload();
        // XposedBridge.log("Loaded app: " + lpparam.packageName + " " + settings.isStarted() + " " + settings.getLat() + " " + settings.getLng());
        // mApp = lpparam.packageName;
        // // if (lpparam.packageName.equals("com.diycircuits.gpsfake"))
        // if (!lpparam.packageName.equals("jp.co.mixi.monsterstrike"))
        // 	return;
    }

    private void updateLocation() {
//        double x = (double) (rand.nextInt(10) - 5);
//        double y = (double) (rand.nextInt(10) - 5);
//        double dlat = x / earth;
//        double dlng = y / ( earth * Math.cos(pi * settings.getLat() / 180.0));
        newlat = settings.getLat();// + (dlat * 180.0 / pi);
        newlng = settings.getLng();// + (dlng * 180.0 / pi);
        //XposedBridge.log("Location Manager New Loc : " + newlat + " " + newlng);
    }

    private void handleGetSystemService(String name, Object instance) {
        if (name.equals(Context.LOCATION_SERVICE)) {
            if (!mLocationManagerHooked) {
                String packageName = AndroidAppHelper.currentPackageName();
                if (packageName.equals(HOOK_TARGET)) {
                    XposedBridge.log("Hook Location Manager " + packageName + " " + instance.getClass().getName());
                    try {
                        Class<?> hookClass = null;
                        hookClass = findClass(instance.getClass().getName(), null);
                        if (hookClass == null)
                            throw new ClassNotFoundException(instance.getClass().getName());

                        Class<?> clazz = hookClass;
                        for (Method method : clazz.getDeclaredMethods()) {
                            int m = method.getModifiers();
                            if (method != null && Modifier.isPublic(m) && !Modifier.isStatic(m)) {
                                // XposedBridge.log("Location Manager Method Name " + method.getName());
                                XposedBridge.hookMethod(method, mMethodHook);
                            }
                        }

                    } catch (Exception ex) {
                    }
                }
                mLocationManagerHooked = true;
            }
        }
    }

    private void hookSystemService(String context) {
        try {
            XC_MethodHook methodHook = new XC_MethodHook() {
                @Override
                protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                }

                @Override
                protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                    if (!param.hasThrowable())
                        try {
                            if (param.args.length > 0 && param.args[0] != null) {
                                // XposedBridge.log("Hook Method : " + mInstance + " " + mApp + " " + packageName);
                                String name = (String) param.args[0];
                                Object instance = param.getResult();
                                if (name != null && instance != null) {
                                    handleGetSystemService(name, instance);
                                }
                            }
                        } catch (Throwable ex) {
                            throw ex;
                        }
                }
            };

            Set<XC_MethodHook.Unhook> hookSet = new HashSet<XC_MethodHook.Unhook>();

            Class<?> hookClass = null;
            try {
                hookClass = findClass(context, null);
                if (hookClass == null)
                    throw new ClassNotFoundException(context);
                // XposedBridge.log("Zygote Context Find Class Done");
            } catch (Exception ex) {
                // XposedBridge.log("Zygote Context Impl Exception " + ex);
            }

            // XposedBridge.log("Zygote Context Find Class " + hookClass);
            Class<?> clazz = hookClass;
            while (clazz != null) {
                for (Method method : clazz.getDeclaredMethods()) {
                    if (method != null && method.getName().equals("getSystemService")) {
                        hookSet.add(XposedBridge.hookMethod(method, methodHook));
                    }
                }
                clazz = (hookSet.isEmpty() ? clazz.getSuperclass() : null);
            }
        } catch (Exception ex) {
            // XposedBridge.log("Zygote Context Hook Exception " + ex);
        }
    }

    @Override
    public void initZygote(StartupParam startupParam) throws Throwable {
        hookSystemService("android.app.ContextImpl");
        hookSystemService("android.app.Activity");
    }

    private Timer mTimer;
    public void startRunning() {
        if(mTimer != null) {
            return;
        }
        mTimer = new Timer();
        mTimer.scheduleAtFixedRate(new TimerTask() {
            @Override public void run() {
                if (mLocationListener != null) {
                    Location l = new Location("gps");
                    updateLocation();
                    //XposedBridge.log("Location updateLocation : " + newlat + " " + newlng);
                    l.setTime(new Long(System.currentTimeMillis()));
                    l.setLatitude(newlat);
                    l.setLongitude(newlng);
                    l.setAccuracy((float) 23.3);
                    //mLocationListener.onLocationChanged(l);
                    XposedHelpers.callMethod(mLocationListener, "onLocationChanged", l);
                }
            }
        }, 500, 1000);
    }

    public void stopRunning() {
        if (mTimer == null) {
            return;
        }
        mTimer.cancel();
        mTimer = null;
    }

}
