package io.automata.pokebrowser;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;

public class MainActivity extends AppCompatActivity {

    private MainApp mApplication;
    public static String TAG = "AutomataCommon";
    public static int OVERLAY_PERMISSION_REQ_CODE = 2428;
    public static int WRITE_PERMISSION_REQ_CODE = 2429;
    public boolean mIsAllowWindow = false;
    ListView mListView;
    private ArrayAdapter<String> mListAdapter ;

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mApplication = (MainApp) getApplication();
        mApplication.sendTrackerScreen(getClass().getSimpleName());

        setContentView(R.layout.activity_main);
        RelativeLayout r = (RelativeLayout) findViewById(R.id.main_view);
        ImageButton startBtn = (ImageButton) findViewById(R.id.btn_start_browser);
        final TextView textView = (TextView) findViewById(R.id.main_info_text);
        mListView = (ListView) findViewById(R.id.mainListView);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (!mIsAllowWindow) {
                    textView.setText("Please allow overlay permission（For Android 6.0 up）");
                    mApplication.sendTrackerEvent("Click", "MainActivityEvent", "NotAllowWindow");
                } else {
                    startService(new Intent(getBaseContext(), WindowService.class));
                    textView.setText("Enjoy GPS Runner");
                    mApplication.sendTrackerEvent("Click", "MainActivityEvent", "GetWindowOverlaySuccess");
                }
            }
        });
//        r.setOnClickListener(new View.OnClickListener() {
//            @Override public void onClick(View v) {
//
//            }
//        });
        if (android.os.Build.VERSION.SDK_INT >= 23) {
            if (!Settings.canDrawOverlays(this)) {
                mIsAllowWindow = false;
                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                    Uri.parse("package:" + getPackageName()));
                startActivityForResult(intent, OVERLAY_PERMISSION_REQ_CODE);
            } else {
                mIsAllowWindow = true;
            }
        } else {
            mIsAllowWindow = true;
        }
        requestPermission();
        showListView();
    }

    public void showListView() {
//        ArrayList<String> packageList;
//        final PackageManager pm = getPackageManager();
//        List<ApplicationInfo> packages = pm.getInstalledApplications(PackageManager.GET_META_DATA);
//        for (ApplicationInfo packageInfo : packages) {
//            Log.d(TAG, "Installed package :" + packageInfo.packageName);
//            Log.d(TAG, "Source dir : " + packageInfo.sourceDir);
//            Log.d(TAG, "Launch Activity :" + pm.getLaunchIntentForPackage(packageInfo.packageName));
//        }
        mListView = (ListView) findViewById( R.id.mainListView );

        String[] planets = new String[] { "com.nianticlabs.pokemongo"};
        ArrayList<String> planetList = new ArrayList<String>();
        planetList.addAll( Arrays.asList(planets) );

        mListAdapter = new ArrayAdapter<String>(this, R.layout.text_view, planetList);
        // Set the ArrayAdapter as the ListView's adapter.
        mListView.setAdapter( mListAdapter );
    }

    private void requestPermission() {
        String[] reqs = new String[]{};

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
            || ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (android.os.Build.VERSION.SDK_INT >= 16)
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        }
    }

    @Override protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (android.os.Build.VERSION.SDK_INT >= 23) {
            if (requestCode == OVERLAY_PERMISSION_REQ_CODE) {
                if (!Settings.canDrawOverlays(this)) {
                    mIsAllowWindow = false;
                } else {
                    mIsAllowWindow = true;
                }
            }
        } else {
            mIsAllowWindow = true;
        }
    }

}

