package io.automata.pokebrowser;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Created by Andy on 2016/9/7.
 */
public class BroadcastReceiver extends android.content.BroadcastReceiver {

    @Override public void onReceive(Context context, Intent intent) {
        //Log.d("onReceive", intent.getAction());
        if (intent.getAction().equals(LocationHooker.BROADCAST_ACTION_INVENTORY)) {
            String data = intent.getStringExtra("data");
            if (data != null) {
                Settings settings = new Settings(context);
                settings.setInventory(data);
                //Log.d("onReceive", data);
            }
        }
    }

}
