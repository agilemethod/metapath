AUTOMATA_INCLUDE=${AUTOMATA_INCLUDE}
AUTOMATA_SRC=${AUTOMATA_SRC}

if [ $AUTOMATA_INCLUDE="" ] || [ $AUTOMATA_SRC="" ]; then
	echo "Please setup the user-defined variables first."
	exit -1
fi

# back to HOME directory
cd ~

# install opencv for android
wget http://downloads.sourceforge.net/project/opencvlibrary/opencv-android/3.0.0/OpenCV-3.0.0-android-sdk-1.zip?r=&ts=1456072160&use_mirror=nchc
unzip OpenCV-3.0.0-android-sdk-1.zip
mkdir AndroidStudio
mv OpenCV-android-sdk AndroidStudio

# back to project directory
cd -
cd app/src/main/jni
ln -s ${AUTOMATA_INCLUDE} include
cd src
ln -s ${AUTOMATA_SRC}/core core
ln -s ${AUTOMATA_SRC}/human human
ln -s ${AUTOMATA_SRC}/matapath matapath
